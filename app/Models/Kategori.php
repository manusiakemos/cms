<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Kategori
 *
 * @property int $kategori_id
 * @property string|null $kategori_nama
 * @property string|null $kategori_aktif
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newQuery()
 * @method static \Illuminate\Database\Query\Builder|Kategori onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Kategori withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Kategori withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Models\Kategori
 *
 * @property int $kategori_id
 * @property string|null $kategori_nama
 * @property string|null $kategori_aktif
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newQuery()
 * @method static \Illuminate\Database\Query\Builder|Kategori onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKategoriNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Kategori withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Kategori withoutTrashed()
 * @mixin \Eloquent
 */
class Kategori extends Model
{
    protected $table = "kategori";

    protected $primaryKey = "kategori_id";

    use SoftDeletes;
}
