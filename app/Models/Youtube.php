<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Youtube extends Model
{
    protected $table = "youtube";

    protected $primaryKey = "id";

    use SoftDeletes;
}
