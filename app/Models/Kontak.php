<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Kontak
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak newQuery()
 * @method static \Illuminate\Database\Query\Builder|Kontak onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak query()
 * @method static \Illuminate\Database\Query\Builder|Kontak withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Kontak withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $html
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereUpdatedAt($value)
 */
/**
 * App\Models\Kontak
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak newQuery()
 * @method static \Illuminate\Database\Query\Builder|Kontak onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak query()
 * @method static \Illuminate\Database\Query\Builder|Kontak withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Kontak withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $html
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kontak whereUpdatedAt($value)
 */
class Kontak extends Model
{
    protected $table = "kontak";

    protected $primaryKey = "id";

    use SoftDeletes;
}
