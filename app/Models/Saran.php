<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Saran
 *
 * @property int $saran_id
 * @property string|null $saran_content
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Saran newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran newQuery()
 * @method static \Illuminate\Database\Query\Builder|Saran onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran query()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereSaranContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereSaranId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Saran withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Saran withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Models\Saran
 *
 * @property int $saran_id
 * @property string|null $saran_content
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Saran newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran newQuery()
 * @method static \Illuminate\Database\Query\Builder|Saran onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran query()
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereSaranContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereSaranId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Saran whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Saran withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Saran withoutTrashed()
 * @mixin \Eloquent
 */
class Saran extends Model
{
    protected $table = "saran";

    protected $primaryKey = "saran_id";

//    use SoftDeletes;
}
