<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Halaman
 *
 * @property int $id
 * @property string|null $custom
 * @property string|null $url
 * @property string|null $judul
 * @property string|null $isi
 * @property string|null $aktif
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman newQuery()
 * @method static \Illuminate\Database\Query\Builder|Halaman onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman query()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|Halaman withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Halaman withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $gambar
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereGambar($value)
 */
/**
 * App\Models\Halaman
 *
 * @property int $id
 * @property string|null $custom
 * @property string|null $url
 * @property string|null $judul
 * @property string|null $isi
 * @property string|null $aktif
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman newQuery()
 * @method static \Illuminate\Database\Query\Builder|Halaman onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman query()
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|Halaman withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Halaman withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $gambar
 * @method static \Illuminate\Database\Eloquent\Builder|Halaman whereGambar($value)
 */
class Halaman extends Model
{
    protected $table = "halaman";

    protected $primaryKey = "id";

//    use SoftDeletes;
}
