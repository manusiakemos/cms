<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Katalog
 *
 * @property int $katalog_id
 * @property int|null $kategori_id
 * @property string|null $nomor
 * @property string|null $judul
 * @property string|null $filename
 * @property string|null $Tahun
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog newQuery()
 * @method static \Illuminate\Database\Query\Builder|Katalog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereKatalogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Katalog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Katalog withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog joinData()
 * @property string|null $tahun
 */
/**
 * App\Models\Katalog
 *
 * @property int $katalog_id
 * @property int|null $kategori_id
 * @property string|null $nomor
 * @property string|null $judul
 * @property string|null $filename
 * @property string|null $Tahun
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog newQuery()
 * @method static \Illuminate\Database\Query\Builder|Katalog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereKatalogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Katalog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Katalog withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Katalog joinData()
 * @property string|null $tahun
 */
class Katalog extends Model
{
    protected $table = "katalog";

    protected $primaryKey = "katalog_id";

    public function scopeJoinData($query)
    {
        return $query->leftJoin("kategori", "kategori.kategori_id", "=", "katalog.kategori_id");
    }

//    use SoftDeletes;
}
