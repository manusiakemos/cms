<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Berita
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newQuery()
 * @method static \Illuminate\Database\Query\Builder|Berita onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita query()
 * @method static \Illuminate\Database\Query\Builder|Berita withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Berita withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $user_id
 * @property int|null $kategori_id
 * @property string|null $judul
 * @property string|null $url
 * @property string|null $gambar
 * @property string|null $isi
 * @property int|null $hit
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereGambar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUserId($value)
 * @property int $berita_id
 * @property int|null $aktif
 * @method static \Illuminate\Database\Eloquent\Builder|Berita joinData()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereBeritaId($value)
 */
/**
 * App\Models\Berita
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita newQuery()
 * @method static \Illuminate\Database\Query\Builder|Berita onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita query()
 * @method static \Illuminate\Database\Query\Builder|Berita withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Berita withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $user_id
 * @property int|null $kategori_id
 * @property string|null $judul
 * @property string|null $url
 * @property string|null $gambar
 * @property string|null $isi
 * @property int|null $hit
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereGambar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereUserId($value)
 * @property int $berita_id
 * @property int|null $aktif
 * @method static \Illuminate\Database\Eloquent\Builder|Berita joinData()
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereAktif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berita whereBeritaId($value)
 */
class Berita extends Model
{
    protected $table = "berita";

    protected $primaryKey = "id";


    public function kategori_berita()
    {
        return $this->belongsTo(KategoriBerita::class, "kategori_id", "kategori_id");
    }

    public function user_berita()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function scopeJoinData($query)
    {
        return $query->leftJoin("users", "users.id", "=", "berita.user_id")
            ->leftJoin("kategori_berita", "berita.kategori_id", "=", "kategori_berita.kategori_id");
    }

//    use SoftDeletes;
}
