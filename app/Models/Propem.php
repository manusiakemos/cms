<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Propem
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Propem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Propem newQuery()
 * @method static \Illuminate\Database\Query\Builder|Propem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Propem query()
 * @method static \Illuminate\Database\Query\Builder|Propem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Propem withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $pengusul_id
 * @property string|null $judul
 * @property string|null $no
 * @property string|null $tanggal
 * @property string|null $tahun
 * @property string|null $file
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem wherePengusulId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereTanggal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem joinData()
 */
/**
 * App\Models\Propem
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Propem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Propem newQuery()
 * @method static \Illuminate\Database\Query\Builder|Propem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Propem query()
 * @method static \Illuminate\Database\Query\Builder|Propem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Propem withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $pengusul_id
 * @property string|null $judul
 * @property string|null $no
 * @property string|null $tanggal
 * @property string|null $tahun
 * @property string|null $file
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem wherePengusulId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereTanggal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Propem joinData()
 */
class Propem extends Model
{
    protected $table = "propem";

    protected $primaryKey = "id";

//    use SoftDeletes;
    public function scopeJoinData($query)
    {
        return $query->join('propem_pengusul', 'propem_pengusul.id', '=', 'propem.pengusul_id');
    }
}
