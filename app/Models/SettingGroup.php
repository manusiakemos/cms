<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SettingGroup
 *
 * @property int $id
 * @property string|null $group
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup newQuery()
 * @method static \Illuminate\Database\Query\Builder|SettingGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SettingGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SettingGroup withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Models\SettingGroup
 *
 * @property int $id
 * @property string|null $group
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup newQuery()
 * @method static \Illuminate\Database\Query\Builder|SettingGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SettingGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SettingGroup withoutTrashed()
 * @mixin \Eloquent
 */
class SettingGroup extends Model
{
    protected $table = "setting_group";

    protected $primaryKey = "id";

    use SoftDeletes;
}
