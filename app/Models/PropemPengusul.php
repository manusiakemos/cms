<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PropemPengusul
 *
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul newQuery()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul query()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $nama
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereUpdatedAt($value)
 */
/**
 * App\Models\PropemPengusul
 *
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul newQuery()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul query()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PropemPengusul withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $nama
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropemPengusul whereUpdatedAt($value)
 */
class PropemPengusul extends Model
{
    protected $table = "propem_pengusul";

    protected $primaryKey = "id";

    use SoftDeletes;
}
