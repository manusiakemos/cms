<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Regulasi
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi newQuery()
 * @method static \Illuminate\Database\Query\Builder|Regulasi onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi query()
 * @method static \Illuminate\Database\Query\Builder|Regulasi withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Regulasi withoutTrashed()
 * @mixin \Eloquent
 * @property int $reg_id
 * @property string|null $kategori_id
 * @property string|null $reg_judul
 * @property string|null $reg_nomor
 * @property string|null $reg_filename
 * @property string|null $reg_tahun
 * @property string|null $reg_tanggal
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegTanggal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi joinKategori()
 */
/**
 * App\Models\Regulasi
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi newQuery()
 * @method static \Illuminate\Database\Query\Builder|Regulasi onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi query()
 * @method static \Illuminate\Database\Query\Builder|Regulasi withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Regulasi withoutTrashed()
 * @mixin \Eloquent
 * @property int $reg_id
 * @property string|null $kategori_id
 * @property string|null $reg_judul
 * @property string|null $reg_nomor
 * @property string|null $reg_filename
 * @property string|null $reg_tahun
 * @property string|null $reg_tanggal
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegNomor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereRegTanggal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Regulasi joinKategori()
 */
class Regulasi extends Model
{
    protected $table = "regulasi";

    protected $primaryKey = "reg_id";

//    use SoftDeletes;

    public function scopeJoinKategori($query)
    {
        return $query->join("kategori", "regulasi.kategori_id", "=", "kategori.kategori_id");
    }
}
