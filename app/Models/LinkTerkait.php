<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LinkTerkait
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $gambar
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait newQuery()
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereGambar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereUrl($value)
 */
/**
 * App\Models\LinkTerkait
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $gambar
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait newQuery()
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereGambar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LinkTerkait withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|LinkTerkait whereUrl($value)
 */
class LinkTerkait extends Model
{
    protected $table = "link_terkait";

    protected $primaryKey = "id";

    use SoftDeletes;
}
