<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Pengumuman
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newQuery()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman query()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Models\Pengumuman
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newQuery()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman query()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Pengumuman withoutTrashed()
 * @mixin \Eloquent
 */
class Pengumuman extends Model
{
    protected $table = "pengumuman";

    protected $primaryKey = "id";

    use SoftDeletes;
}
