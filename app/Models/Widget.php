<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Widget
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newQuery()
 * @method static \Illuminate\Database\Query\Builder|Widget onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget query()
 * @method static \Illuminate\Database\Query\Builder|Widget withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Widget withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $html
 * @property int|null $fixed
 * @property string|null $css_cdn
 * @property string|null $js_cdn
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereCssCdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereFixed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereJsCdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereUpdatedAt($value)
 */
/**
 * App\Models\Widget
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget newQuery()
 * @method static \Illuminate\Database\Query\Builder|Widget onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Widget query()
 * @method static \Illuminate\Database\Query\Builder|Widget withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Widget withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $html
 * @property int|null $fixed
 * @property string|null $css_cdn
 * @property string|null $js_cdn
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereCssCdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereFixed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereJsCdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Widget whereUpdatedAt($value)
 */
class Widget extends Model
{
    protected $table = "widget";

    protected $primaryKey = "id";

    use SoftDeletes;
}
