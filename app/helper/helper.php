<?php

use Carbon\Carbon;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

function replaceArrayString($x)
{
    $y = preg_replace("(\]|\[|'|\")", "", $x);
    return $y;
}

function my_upload_file($file, $path = "uploads", $withpath = false)
{
    $ext = $file->getClientOriginalExtension();
    $filename = Str::random() . '.' . $ext;
    $file->move($path, $filename);
    if ($withpath) {
        return asset($path . "/" . $filename);
    } else {
        return $filename;
    }
}

function resizeGambar($gambar, $lokasi, $width = 680, $height = 350, $thumbWidth = 200, $thumbHeight = 100)
{
    if(file_exists(public_path($lokasi)) ==  false){
        mkdir(public_path($lokasi));
    }
//    uploads/slide/
    $namaGambar = Str::random(60) . '.' . $gambar->getClientOriginalExtension();
    $lokasiGambar = $lokasi . $namaGambar;
    $lokasiThumbs = $lokasi . 'thumbs_' . $namaGambar;

    //  Crop
    $gambarnya = Image::make($gambar);
    $gambarnya->resize($width, $height);
    $gambarnya->save($lokasiGambar);


    //  Gambar Kecil
    $gambarnya->fit($thumbWidth, $thumbHeight);
    $gambarnya->save($lokasiThumbs);
    return $namaGambar;
}

function resizeAspectGambar($gambar, $lokasi, $width = null, $height = null, $thumbWidth = null, $thumbHeight = null)
{
    if(file_exists(public_path($lokasi)) ==  false){
        mkdir(public_path($lokasi));
    }
    $namaGambar = Str::random(60) . '.' . $gambar->getClientOriginalExtension();
    $lokasiGambar = $lokasi . $namaGambar;
    $gambarnya = Image::make($gambar);
    $gambarnya->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
    });
    $gambarnya->save($lokasiGambar);
    return $namaGambar;
}

function my_asset($file, $dir = "/themes/magazine/")
{
    return asset($dir . $file);
}

function base64_to_image($data, $path)
{
    list($type, $data) = explode(';', $data);
    list(, $data) = explode(',', $data);
    $data = base64_decode($data);

    $up = File::put(public_path($path), $data);

    return $up;
}

function if_empty($str, $out = "-")
{
    if ($str == null) {
        return $out;
    }
    return $str;
}

function boolean_text($bool, $true = "aktif", $false = "tidak aktif")
{
    if ($bool == true) {
        return $true;
    } else {
        return $false;
    }
}

function text_to_boolean($text, $true = "aktif", $false = "tidak aktif")
{
    if ($text == $true) {
        return true;
    } else {
        return false;
    }
}

function getBulanFromDate($date, $year = false)
{
    $dt = Carbon::parse($date);
    if ($year) {
        return bulan($dt->month) . ' ' . $dt->year;
    }
    return bulan($dt->month);
}

function responseJson($message, $data = null, $status = true, $text = 'success', $statusCode = 200)
{
    return response(['status' => $status, 'text' => $text, 'message' => $message, 'data' => $data], $statusCode);
}

function bulan($month)
{
    if ($month == 1) {
        $bulan = 'januari';
    } else if ($month == 2) {
        $bulan = 'februari';
    } else if ($month == 3) {
        $bulan = 'maret';
    } else if ($month == 4) {
        $bulan = 'april';
    } else if ($month == 5) {
        $bulan = 'mei';
    } else if ($month == 6) {
        $bulan = 'juni';
    } else if ($month == 7) {
        $bulan = 'juli';
    } else if ($month == 8) {
        $bulan = 'agustus';
    } else if ($month == 9) {
        $bulan = 'september';
    } else if ($month == 10) {
        $bulan = 'oktober';
    } else if ($month == 11) {
        $bulan = 'november';
    } else if ($month == 12) {
        $bulan = 'desember';
    }

    return $bulan;
}

function waktu($timestamps)
{
    $dt = Carbon::parse($timestamps);
    return $dt->hour . ":" . $dt->minute;
}

function tanggal($timestamps, $separator)
{
    $dt = Carbon::parse($timestamps);

    return "{$dt->day}/{$dt->month}/$dt->year";
}

function tanggal_indo($timestamps, $tampilkan_hari = true, $tampilkan_waktu = false, $hanyaHari = false)
{
    $dt = Carbon::parse($timestamps);
    $hari = $dt->dayOfWeek;
    if ($hari == 1) {
        $hari = 'Senin';
    } else if ($hari == 2) {
        $hari = 'Selasa';
    } else if ($hari == 3) {
        $hari = 'Rabu';
    } else if ($hari == 4) {
        $hari = 'Kamis';
    } else if ($hari == 5) {
        $hari = 'Jumat';
    } else if ($hari == 6) {
        $hari = 'Sabtu';
    } else {
        $hari = 'Minggu';
    }

    if ($hanyaHari) {
        return $hari;
    }

    if ($tampilkan_hari == false) {
        $hari = "";
    }

    $day = $dt->day;
    $month = $dt->month;

    if ($month == 1) {
        $bulan = 'januari';
    } else if ($month == 2) {
        $bulan = 'februari';
    } else if ($month == 3) {
        $bulan = 'maret';
    } else if ($month == 4) {
        $bulan = 'april';
    } else if ($month == 5) {
        $bulan = 'mei';
    } else if ($month == 6) {
        $bulan = 'juni';
    } else if ($month == 7) {
        $bulan = 'juli';
    } else if ($month == 8) {
        $bulan = 'agustus';
    } else if ($month == 9) {
        $bulan = 'september';
    } else if ($month == 10) {
        $bulan = 'oktober';
    } else if ($month == 11) {
        $bulan = 'november';
    } else if ($month == 12) {
        $bulan = 'desember';
    }

    $bulan = ucwords($bulan);

    $tahun = $dt->year;

    $waktu = $dt->format("H:i:s");

    if ($tampilkan_waktu) {
        $tanggal = "$hari $day $bulan $tahun $waktu";
    } else {
        $tanggal = "$hari $day $bulan $tahun";
    }

    return $tanggal;
}

function buatToken($nikTps)
{
    $cipher = "idea-ofb";
    $iv = substr(hash('sha256', md5($nikTps)), 0, openssl_cipher_iv_length($cipher));
    $hasil = openssl_encrypt($nikTps, $cipher, md5("e-voting tabalong"), 0, $iv);

    return $hasil;
}

function generateToken()
{
    return mt_rand(100000, 999999);
}

function validateToken($token, $nikTps)
{
    $cipher = "idea-ofb";
    $iv = substr(hash('sha256', md5($nikTps)), 0, openssl_cipher_iv_length($cipher));
    $hasil = openssl_decrypt($token, $cipher, md5("e-voting tabalong"), 0, $iv);

    return $hasil == $nikTps;
}

function catzToken($nik, $periode)
{
    return md5(md5($nik . $periode));
}

function makeTanggal($date)
{
//    05|02|1987
    $explode = explode("|", $date);
    return "$explode[2]-$explode[1]-$explode[0]";
}

function rupiah($angka)
{
    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
}

/**
 * @param $name
 * @param $id
 * @param array $links_additional
 * @return array
 */
function generate_links($name, $id, $links_additional = [])
{
    $links = [
        'store' => route($name . ".store"),
        'show' => route($name . '.show', $id),
        'edit' => route($name . '.edit', $id),
        'update' => route($name . '.update', $id),
        'destroy' => route($name . '.destroy', $id),
    ];
    if (count($links_additional) > 0) {
        array_push($links, $links_additional);
    }
    return auth()->check() ? $links : [];
}

function generate_links_api($name, $id, $links_additional = [])
{
    $links = [
        'store' => route($name . ".store"),
        'show' => route($name . '.show', $id),
        'update' => route($name . '.update', $id),
        'destroy' => route($name . '.destroy', $id),
    ];

    if (count($links_additional) > 0) {
        /*foreach($links_additional as $key => $link){
            array_merge($links, $link);
        }*/
        $links = array_merge($links, $links_additional);
    }

    return $links;
}

function template_view($view, $variables = [])
{

    $template = config('app.template');
    $path = "web_" . $template . ".web." . $view;
    if (count($variables) > 0) {
        return view($path, $variables);
    }
    return view($path);
}
