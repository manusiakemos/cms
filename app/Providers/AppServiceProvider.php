<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('crud.start_app')) {
            Paginator::defaultView('vendor.pagination.bootstrap-4');

            Paginator::defaultSimpleView('vendor.pagination.simple-bootstrap-4');

            JsonResource::withoutWrapping();

            Schema::defaultStringLength(191);

            if(Schema::hasTable('setting')){
                $setting = Setting::joinData()->where('active', 1)->get();

                $setting = $setting->groupBy('group');

                $configApp = [];
                foreach ($setting as $key => $value) {
                    $configApp[$key] = [];
                    foreach ($value as $item) {
                        $configApp[$key][$item->name] = $item->value ? $item->value : $item->image;
                    }
                }
                config([
                    'configApp' => $configApp
                ]);

                View::share([
                    'configApp' => $configApp,
                ]);
            }
        }
    }
}
