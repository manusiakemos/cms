<?php

namespace App\View\Components;

use Illuminate\View\Component;

class WebHeader extends Component
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $menu;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $menu = file_get_contents(database_path("/json/menu.json"));
        $this->menu = json_decode($menu);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.web-header', [
            'menus' => $this->menu
        ]);
    }
}
