<?php

namespace App\View\Components;

use App\Models\Setting;
use App\Models\Youtube;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class WebYoutube extends Component
{
    /**
     * @var mixed
     */

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = Cache::rememberForever('youtube', function () {
            return Youtube::all();
        });
        return view('components.web-youtube', compact('data'));
    }
}
