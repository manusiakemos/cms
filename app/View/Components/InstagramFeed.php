<?php

namespace App\View\Components;

use Dymantic\InstagramFeed\Profile;
use Illuminate\View\Component;

class InstagramFeed extends Component
{
    /**
     * @var int
     */
    public $limit;

    /**
     * Create a new component instance.
     *
     * @param int $limit
     */
    public function __construct($limit = 20)
    {
        //
        $this->limit = $limit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $limit = $this->limit;

        $feed = Profile::where('username', config('instagram-feed.username'))
            ->first()
            ->feed($limit);
        return view('components.instagram-feed', compact('feed', 'limit'));
    }
}
