<?php

namespace App\View\Components;

use App\Models\Slider;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class SliderWeb extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $sliders = Cache::rememberForever("sliders", function (){
            return Slider::where('aktif', 1)->get();
        });
        return view('components.slider-web', compact('sliders'));
    }
}
