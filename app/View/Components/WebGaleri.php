<?php

namespace App\View\Components;

use App\Models\Gallery;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class WebGaleri extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    protected $limit;
    public function __construct($limit)
    {
        $this->limit = $limit;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $gallery = Cache::rememberForever("gallery", function (){
            return Gallery::limit($this->limit)->latest()->get();
        });
        return view('components.web-galeri',compact('gallery'));
    }
}
