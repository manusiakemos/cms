<?php

namespace App\View\Components;

use App\Models\Berita;
use Illuminate\View\Component;

class BeritaWeb extends Component
{
    /**
     * @var int|mixed
     */
    public $limit;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($limit = 6, $type = 'terbaru')
    {
        $this->limit = $limit;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $select = ["berita.*", "users.name", "kategori_berita.nama"];
        $data = Berita::select($select)->joinData()->limit($this->limit);
        if($this->type == 'terbaru'){
            $data = $data->orderBy('berita.created_at', 'desc')->get();
        }else{
            $data = $data->orderBy('hit', 'desc')->get();
        }
        $type = $this->type;
        return view('components.berita-web', compact('data','type'));
    }
}
