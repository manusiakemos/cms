<?php

namespace App\View\Components;

use App\Models\Kontak;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class WebFooter extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $kontak = Cache::rememberForever("kontak", function (){
           return Kontak::all();
        });
        return view('components.web-footer', compact('kontak'));
    }
}
