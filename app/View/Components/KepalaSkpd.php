<?php

namespace App\View\Components;

use Illuminate\View\Component;

class KepalaSkpd extends Component
{
    public $ketuaNama;
    public $ketuaImage;
    public $ketuaJabatan;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $ketua = isset( config('configApp')['ketua'] ) ?  config('configApp')['ketua'] : false;
        $this->ketuaNama = isset($ketua['ketua_nama']) && $ketua['ketua_nama'] ? $ketua['ketua_nama'] : false;
        $this->ketuaImage = isset($ketua['ketua_image']) && $ketua['ketua_image'] ? $ketua['ketua_image'] : false;
        $this->ketuaJabatan = isset($ketua['ketua_jabatan']) && $ketua['ketua_jabatan'] ? $ketua['ketua_jabatan'] : false;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = false;
        if($this->ketuaNama && $this->ketuaImage && $this->ketuaJabatan){
            $data = true;
        }
        return view('components.kepala-skpd',[
            'data' => $data,
            'ketua_nama' => $this->ketuaNama,
            'ketua_image' => $this->ketuaImage,
            'ketua_jabatan' => $this->ketuaJabatan,
        ]);
    }
}
