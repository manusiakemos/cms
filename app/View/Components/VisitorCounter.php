<?php

namespace App\View\Components;

use Illuminate\View\Component;

class VisitorCounter extends Component
{
    /**
     * @var mixed
     */
    public $visitor;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $file = file_get_contents(database_path('/json/visitor.json'));
        $this->visitor = json_decode($file);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.visitor-counter',[
            'visitor' => $this->visitor
        ]);
    }
}
