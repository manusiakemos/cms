<?php

namespace App\View\Components;

use App\Models\Halaman;
use App\Models\Kategori;
use App\Models\KategoriBerita;
use App\Models\PropemPengusul;
use App\Models\Regulasi;
use App\Models\SettingGroup;
use Illuminate\View\Component;

class SelectComponent extends Component
{
    public $case;
    public $selected;
    public $name;
    public $textValue;
    public $textLabel;
    public $id;
    public $errorContainer;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($case, $name, $id, $selected = "", $errorContainer = "", $textValue = "value", $textLabel = "text")
    {
        $this->textValue = $textValue;
        $this->textLabel = $textLabel;
        $this->id = $id;
        $this->case = $case;
        $this->selected = $selected;
        $this->name = $name;
        $this->errorContainer = $errorContainer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $options = [];
        $placeholder = "Pilih Salah Satu";
        $value = "value";
        $text = "text";
        $name = $this->name;
        $id = $this->id;
        $selected = $this->selected;
        $textValue = $this->textValue;
        $textLabel = $this->textLabel;
        $errorContainer = $this->errorContainer;

        switch ($this->case) {
            case 'group_id':
                $options = SettingGroup::all();
                $placeholder = "Pilih Group";
                break;
            case 'setting_type':
                $options = $this->typeSettingOptions();
                $placeholder = "Pilih Tipe";
                break;
            case 'status':
                $options = $this->statusOptions();
                $placeholder = "Pilih Status";
                break;
            case 'role':
                $options = $this->roleOptions();
                $placeholder = "Pilih Role";
                break;
            case 'kategori':
                $options = Kategori::all();
                $placeholder = "Pilih Kategori";
                break;
            case 'pengusul_id':
                $options = PropemPengusul::all();
                $placeholder = "Pilih Pengusul";
                break;
            case 'kategori_berita':
                $options = KategoriBerita::all();
                $placeholder = "Pilih Kategori Berita";
                break;
            case 'halaman':
                $options = Halaman::all();
                $placeholder = "Pilih Halaman";
                break;
            case 'reg_tahun':
                $options = Regulasi::select("reg_tahun")->distinct()->orderBy('reg_tahun', 'asc')->get();
//                    ->pluck('reg_tahun');
                $placeholder = "Pilih Tahun";
                break;
        }
        return view('components.select-component', compact('options', 'placeholder', 'value', 'text', 'selected', 'name', 'id', 'errorContainer', 'textValue', 'textLabel'));
    }

    private function typeSettingOptions()
    {
        $options = [
            [
                'value' => "text",
                'text' => 'Text',
            ],
            [
                'value' => "image",
                'text' => 'Image',
            ],
            [
                'value' => "textarea",
                'text' => 'TextArea',
            ]
        ];
        $options = json_decode(json_encode($options), FALSE);
        return $options;
    }

    private function statusOptions()
    {
        $options = [
            [
                'value' => "1",
                'text' => 'Aktif',
            ],
            [
                'value' => "0",
                'text' => 'Tidak Aktif',
            ]
        ];
        $options = json_decode(json_encode($options), FALSE);
        return $options;
    }

    private function roleOptions()
    {
        $options = [
            [
                'value' => "admin",
                'text' => 'Admin',
            ],
            [
                'value' => "super-admin",
                'text' => 'Super Admin',
            ]
        ];
        $options = json_decode(json_encode($options), FALSE);
        return $options;
    }
}
