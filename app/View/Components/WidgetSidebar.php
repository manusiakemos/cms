<?php

namespace App\View\Components;

use Illuminate\View\Component;

class WidgetSidebar extends Component
{
    /**
     * @var false|mixed
     */
    public $long;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($long = false)
    {
        $this->long = $long;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.widget-sidebar', ['long' => $this->long]);
    }
}
