<?php

namespace App\View\Components;

use App\Models\Pengumuman;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class PengumumanComponent extends Component
{
    public $limit;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($limit = 3)
    {
        $this->limit = $limit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $data = Cache::rememberForever("pengumuman", function (){
            return Pengumuman::limit($this->limit)->latest()->where('aktif', 1)->get();
        });
        return view('components.pengumuman-component', compact('data'));
    }
}
