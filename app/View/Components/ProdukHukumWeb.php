<?php

namespace App\View\Components;

use App\Models\Regulasi;
use Illuminate\View\Component;

class ProdukHukumWeb extends Component
{
    public $tipe;
    public $limit;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tipe = 'terbaru', $limit=3)
    {
        $this->tipe = $tipe;
        $this->limit = $limit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $data = $this->{$this->tipe}();
        return view('components.produk-hukum-web', compact('data'));
    }

    public function terbaru()
    {
        return Regulasi::joinKategori()->latest('reg_tahun')->limit($this->limit)->get();
    }

    public function terpopuler()
    {

    }
}
