<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Editor extends Component
{
    public $value;
    public $name;
    public $id;
    public $errorContainer;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $id, $value = "", $errorContainer = "")
    {
        $this->id = $id;
        $this->value = $value;
        $this->name = $name;
        $this->errorContainer = $errorContainer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.editor');
    }
}
