<?php

namespace App\View\Components;

use App\Models\LinkTerkait;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\Component;

class WebLinkTerkait extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $data = Cache::rememberForever("linkterkait", function (){
            return  LinkTerkait::all();
        });
        return view('components.web-link-terkait', compact('data'));
    }
}
