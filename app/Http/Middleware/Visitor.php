<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class Visitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $file = database_path('json/visitor.json');
        $db = file_get_contents($file);
        $db = json_decode($db, true);

        $dt = Carbon::parse($db['dt']);

        $this->processJsonData($file,$db, $dt);

        return $next($request);
    }

    private function processJsonData($file, $db, Carbon $dt)
    {
        //handle hari ini
        if($dt->format('Y-m-d') == date('Y-m-d')){
            //jika tanggal sekarang sama dengan tanggal pada hari ini maka increment
            $db['hari_ini'] = $db['hari_ini'] + 1;
        }else{
            //jika tidak maka jadikan 0;
            $db['hari_ini'] = 1;
        }
        //handle bulan ini
        if ($dt->month != date('m')){
            $db['bulan_ini'] = 1;
        }else{
            $db['bulan_ini'] = $db['bulan_ini'] + 1;
        }

        //handle total
        $db['total'] = $db['total'] + 1;

        $db['dt'] = now()->format('Y-m-d');

        file_put_contents($file, json_encode($db));
    }
}
