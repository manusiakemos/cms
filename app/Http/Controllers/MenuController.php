<?php

namespace App\Http\Controllers;

use App\Models\Halaman;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->only(['index']);
        $this->middleware(['auth:web'])->only(['api', 'save']);
    }

    public function api(Request $request)
    {
        if($request->has("case")){
            switch ($request->case){
                case "halaman":
                    return Halaman::all();
                    break;
                case "type":
                    $options = [
                        [
                            'value' => "link",
                            'text' => 'link',
                        ],
                        [
                            'value' => "dropdown",
                            'text' => 'dropdown',
                        ],
                        [
                            'value' => "halaman",
                            'text' => 'halaman',
                        ],
                        [
                            'value' => "eksternal",
                            'text' => 'eksternal',
                        ]
                    ];
                    return $options;
                    break;
                default:
                    return $file = file_get_contents(database_path('json/menu.json'));
                    break;
            }
        }

        return $file = file_get_contents(database_path('json/menu.json'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        return view('menu.index');
    }

    public function save(Request $request)
    {
        $dataJson = json_encode($request->data);
        file_put_contents(database_path("json/menu.json"), $dataJson);
    }
}
