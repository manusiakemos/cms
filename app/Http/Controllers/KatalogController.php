<?php

namespace App\Http\Controllers;

use App\Models\Katalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class KatalogController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Katalog::whereIn("katalog_id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Katalog berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Katalog gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Katalog::joinData();
        /*if ($request->has("katalog_status") && $request->katalog_status != null) {
            $data = $data->where("_katalog.katalog_status","=", $request->katalog_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Katalog $data) {
                    return view("katalog.actions_katalog", ['data' => $data]);
                })
                ->addColumn('checked', function (Katalog $data) {
                    return view("katalog.checked_katalog", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('katalog.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('katalog.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "kategori_id" => [
                "required"
            ],
            "nomor" => [
                "required"
            ],
            "judul" => [
                "required"
            ],
//            "filename" => [
//                "required"
//            ],
            "Tahun" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Katalog;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Katalog berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Katalog gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Katalog $katalog
     * @return \Illuminate\Http\Response
     */
    public function show(Katalog $katalog)
    {
        return view('katalog.show', ['data' => $katalog]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Katalog $katalog
     * @return \Illuminate\Http\Response
     */
    public function edit(Katalog $katalog)
    {
        return view('katalog.edit', ['data' => $katalog], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Katalog $katalog
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Katalog $katalog)
    {

        $rules = [
            "kategori_id" => [
                "required"
            ],
            "nomor" => [
                "required"
            ],
            "judul" => [
                "required"
            ],
            "filename" => [
                "required"
            ],
            "Tahun" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $katalog;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Katalog berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Katalog gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Katalog $katalog
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Katalog $katalog)
    {
        $save = $katalog->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Katalog berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Katalog gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Katalog $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->kategori_id = $request->kategori_id;
        $db->nomor = $request->nomor;
        $db->judul = $request->judul;
        if ($request->hasFile('filename')) {
            $filename = Str::random() . "." . $request->file('filename')->getClientOriginalExtension();
            Storage::putFileAs('uploads', $request->file('filename'), $filename);
            $db->filename = $filename;
        }
        $db->Tahun = $request->Tahun;

        return $db->save();
    }
}
