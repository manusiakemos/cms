<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use App\Models\Gallery;
use App\Models\Halaman;
use App\Models\Katalog;
use App\Models\Pengumuman;
use App\Models\Propem;
use App\Models\Regulasi;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class WebController extends Controller
{
    /**
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function viewRegulasi($nomor,$tahun)
    {

        $regulasi = Regulasi::where('reg_nomor', $nomor)
            ->where('reg_tahun', $tahun)
            ->firstOrFail();
        if ($regulasi) {
            return response()
                ->file(public_path("/uploads/pdf/{$regulasi->reg_filename}"));
        }
        abort('403', 'File Tidak Ditemukan');
    }

    public function pengumuman($slug)
    {
        $data = Pengumuman::find($slug);
        return view('pengumuman', compact('data'));
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function viewBerita($slug)
    {
        $berita = Berita::select(['berita.*', 'nama', 'name'])->joinData()->where('url', $slug)->firstOrFail();
        $berita->hit = $berita->hit +1;
        $berita->save();
        if ($berita) {
            return view('berita_detail', compact('berita'));
        }
    }
    public function viewHalaman($slug)
    {
        $halaman = Halaman::whereUrl($slug)->firstOrFail();
        return view("halaman", compact('halaman'));
    }

    public function produkHukum(Request $request)
    {
        if ($request->ajax()) {

            $data = Regulasi::JoinKategori();
            if ($request->has("tahun") && $request->tahun != null) {
                $data = $data->where("regulasi.reg_tahun","=", $request->tahun);
            }
            if ($request->has("kategori") && $request->kategori != null) {
                $data = $data->where("regulasi.kategori_id","=", $request->kategori);
            }
            return DataTables::of($data)
                ->editColumn('reg_filename', function (Regulasi $data) {
                    return view('regulasi.actions_regulasi', compact('data'));
                })
                ->rawColumns(['reg_filename'])
                ->toJson();
        }
        return view('regulasi');
    }

    public function katalog(Request $request)
    {
        if ($request->ajax()) {

            $data = Katalog::joinData();
            if ($request->has("tahun") && $request->tahun != null) {
                $data = $data->where("katalog.tahun","=", $request->tahun);
            }
            if ($request->has("kategori") && $request->kategori != null) {
                $data = $data->where("katalog.kategori_id","=", $request->kategori);
            }
            return DataTables::of($data)
                ->toJson();
        }
        return view('katalog');
    }

    public function berita(Request $request)
    {
        $berita = Berita::select(['berita.*', 'nama', 'name'])->joinData();
        if (isset($request->s)){
            $berita = $berita->where("judul", "like", "%{$request->s}%")
                ->orWhere("isi", "like", "%{$request->s}%");
        }
        $berita = $berita->latest()->paginate(6);
        return view('berita', compact('berita'));
    }

    public function galeri()
    {
        $galeri = Gallery::all();
        return view('galeri', compact('galeri'));
    }

    public function propem(Request $request)
    {
        if ($request->ajax()) {

            $data = Propem::joinData();
            if ($request->has("tahun") && $request->tahun != null) {
                $data = $data->where("propem.tahun","=", $request->tahun);
            }
            if ($request->has("pengusul") && $request->pengusul != null) {
                $data = $data->where("propem.pengusul_id","=", $request->pengusul);
            }
            return DataTables::of($data)->toJson();
        }
        return view('propem');
    }
}
