<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CssEditorController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin']);
    }

    public function show()
    {
        $cssContent = file_get_contents(public_path('css/web.css'));
        return view('css_editor.show', compact('cssContent'));
    }

    public function save(Request $request)
    {
        $css = $request->input('css');
        $save = file_put_contents(public_path('css/web.css'), $css );
        return $save
            ? responseJson('success', 'css berhasil disimpan')
            : responseJson('error', 'css gagal disimpan');
    }
}
