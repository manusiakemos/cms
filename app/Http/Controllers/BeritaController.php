<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class BeritaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function api(Request $request)
    {
        /* //JIKA ADA DATA YANG DIKIRIMKAN
         if ($request->hasFile('upload')) {
             $file = $request->file('upload'); //SIMPAN SEMENTARA FILENYA KE VARIABLE
             $filename = Str::random() . "." . $file->getClientOriginalExtension();
             Storage::putFileAs('/uploads/media', $file, $filename);

             //KEMUDIAN KITA BUAT RESPONSE KE CKEDITOR
             $ckeditor = $request->input('CKEditorFuncNum');
             $url = asset('uploads/media/' . $filename);
             $msg = 'Image uploaded successfully';
             //DENGNA MENGIRIMKAN INFORMASI URL FILE DAN MESSAGE
             $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

             //SET HEADERNYA
             @header('Content-type: text/html; charset=utf-8');
             return $response;
         }*/
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Berita::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Berita berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Berita gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Berita::with(['kategori_berita']);
        /*if ($request->has("berita_status") && $request->berita_status != null) {
            $data = $data->where("_berita.berita_status","=", $request->berita_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function ( $data) {
                    return view("berita.actions_berita", ['data' => $data]);
                })
                ->editColumn('gambar', function ( $data) {
                    return "<a href='" . asset('uploads/berita/' . $data->gambar) . "'>Lihat Gambar</a>";
                })
                ->addColumn('checked', function ( $data) {
                    return view("berita.checked_berita", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked', 'gambar'])
                ->toJson();
        }

        return view('berita.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('berita.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "kategori_id" => [
                "required"
            ],
            "judul" => [
                "required",
                Rule::unique('berita', 'judul')
            ],
            "gambar" => [
                "required"
            ],
            "isi" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Berita;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Berita berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Berita gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Berita $berita
     * @return \Illuminate\Http\Response
     */
    public function show(Berita $berita)
    {
        return view('berita.show', ['data' => $berita]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Berita $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::findOrFail($id);
        return view('berita.edit', ['data' => $berita], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Berita $berita
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "kategori_id" => [
                "required"
            ],
            "judul" => [
                "required",
                Rule::unique('berita', 'judul')
                    ->ignore($id, 'id')

            ],
            "isi" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = Berita::findOrFail($id);
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Berita berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Berita gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Berita $berita
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $save =  Berita::destroy($id);

       return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Berita berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Berita gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Berita $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->user_id = auth()->id();
        $db->kategori_id = $request->kategori_id;
        $db->judul = $request->judul;
        $db->url = date('Y-m-d') . '-' . Str::slug($request->judul);
        if ($request->hasFile('gambar')) {
            //  Proses Gambar
            if(!$db->id){
                $directory = storage_path('app/uploads/berita');
                if (!file_exists($directory)) {
                    mkdir($directory, 0777, true);
                }
            }
            $filename = resizeGambar($request->file('gambar'), 'uploads/berita/');
            $db->gambar = $filename;
        }
        $db->isi = $request->isi;

        return $db->save();
    }
}
