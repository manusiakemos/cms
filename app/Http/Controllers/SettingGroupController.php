<?php

namespace App\Http\Controllers;

use App\Models\SettingGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SettingGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','role:super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
        * @param Request $request
        * @return \Illuminate\Http\JsonResponse
        * hapus semua
        */
       public function bulkDelete(Request $request)
       {
           $data = collect($request->data);
           $save = SettingGroup::whereIn("id", $data->pluck("id")->unique())->delete();
           return $save
               ? response()->json(['status' => true, 'text' => 'success', 'message' => 'SettingGroup berhasil dihapus'])
               : response()->json(['status' => false, 'text' => 'error', 'message' => 'SettingGroup gagal dihapus'], 500);
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = SettingGroup::query();
        /*if ($request->has("settinggroup_status") && $request->settinggroup_status != null) {
            $data = $data->where("_settinggroup.settinggroup_status","=", $request->settinggroup_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (SettingGroup $data) {
                    return view("settinggroup.actions_settinggroup", ['data' => $data]);
                })
                ->addColumn('checked', function (SettingGroup $data) {
                    return view("settinggroup.checked_settinggroup", ['data' => $data]);
                })
                ->rawColumns(['action','checked'])
                ->toJson();
        }

        return view('settinggroup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settinggroup.create',['modal_title'=>'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
    "group" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = new SettingGroup;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'SettingGroup berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'SettingGroup gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SettingGroup $settinggroup
     * @return \Illuminate\Http\Response
     */
    public function show(SettingGroup $settinggroup)
    {
        return view('settinggroup.show', ['data' => $settinggroup]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SettingGroup $settinggroup
     * @return \Illuminate\Http\Response
     */
    public function edit(SettingGroup $settinggroup)
    {
        return view('settinggroup.edit', ['data' => $settinggroup], ['modal_title'=>'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\SettingGroup $settinggroup
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, SettingGroup $settinggroup)
    {

       $rules = [
    "group" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = $settinggroup;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'SettingGroup berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'SettingGroup gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SettingGroup $settinggroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(SettingGroup $settinggroup)
    {
        $save = $settinggroup->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'SettingGroup berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'SettingGroup gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\SettingGroup $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->group = $request->group;

        return $db->save();
    }
}
