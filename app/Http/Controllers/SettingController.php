<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Setting::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Setting berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Setting gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Setting::joinData();
        if ($request->has("group_id") && $request->group_id != null) {
            $data = $data->where("setting.group_id","=", $request->group_id);
        }
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Setting $data) {
                    return view("setting.actions_setting", ['data' => $data]);
                })
                ->editColumn('fixed', function (Setting $data) {
                    return boolean_text($data->fixed, "Ya", "Tidak");
                })
                ->editColumn('active', function (Setting $data) {
                    return boolean_text($data->active, "Ya", "Tidak");
                })
                ->addColumn('checked', function (Setting $data) {
                    return view("setting.checked_setting", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('setting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "group_id" => [
                "required"
            ],
            "name" => [
                "required","alpha_dash"
            ],
            "value" => [
                "sometimes","nullable"
            ],
            "image" => [
                "sometimes","nullable","image"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Setting;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Setting berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Setting gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        return view('setting.show', ['data' => $setting]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('setting.edit', ['data' => $setting], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Setting $setting
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Setting $setting)
    {

        $rules = [
            "group_id" => [
                "required"
            ],
            "name" => [
                "required","alpha_dash"
            ],
            "value" => [
                "sometimes","nullable"
            ],
            "image" => [
                "sometimes","nullable","image"
            ]
        ];
        $this->validate($request, $rules);

        $db = $setting;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Setting berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Setting gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Setting $setting
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Setting $setting)
    {
       if (auth()->user()->role == 'super-admin'){
           $save = $setting->delete();

           return $save
               ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Setting berhasil dihapus'])
               : response()->json(['status' => false, 'text' => 'error', 'message' => 'Setting gagal dihapus'], 500);
       }else{
          return response()->json(['status' => false, 'text' => 'error', 'message' => 'Setting gagal dihapus'], 200);
       }
    }

    /**
     * @param $request
     * @param \App\Models\Setting $db
     * @return mixed
     */
    private function handleRequest(Request $request, Setting $db)
    {
        $db->group_id = $request->group_id;
        $db->name = $request->name;
        $db->value = $request->value;
        if ($request->hasFile('image')) {
            $filename = Str::random() . "." . $request->file('image')->getClientOriginalExtension();
            Storage::putFileAs('uploads', $request->file('image'), $filename);
            $db->image = $filename;
            $db->type = 'image';
        }else{
            if(!$db->id){
                $db->type = 'text';
            }
        }

        if (auth()->user()->role == 'super-admin'){
            $db->fixed = $request->fixed;
        }else{
            if (!$db->id){
                $db->fixed = 1;
            }
        }

        $db->active = $request->active;

        return $db->save();
    }
}
