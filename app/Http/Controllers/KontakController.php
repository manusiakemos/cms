<?php

namespace App\Http\Controllers;

use App\Models\Kontak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class KontakController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget("kontak");
        $data = collect($request->data);
        $save = Kontak::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kontak berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kontak gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Kontak::query();
        /*if ($request->has("kontak_status") && $request->kontak_status != null) {
            $data = $data->where("_kontak.kontak_status","=", $request->kontak_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Kontak $data) {
                    return view("kontak.actions_kontak", ['data' => $data]);
                })
                ->editColumn('icon', function (Kontak $data) {
                    return "<a href='".asset('uploads/'.$data->icon)."'>Lihat Icon</a>";
                })
                ->addColumn('checked', function (Kontak $data) {
                    return view("kontak.checked_kontak", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked', 'icon'])
                ->toJson();
        }

        return view('kontak.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kontak.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "name" => [
                "required"
            ],
            "icon" => [
                "sometimes", "nullable", "image"
            ],
            "html" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Kontak;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kontak berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kontak gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Kontak $kontak
     * @return \Illuminate\Http\Response
     */
    public function show(Kontak $kontak)
    {
        return view('kontak.show', ['data' => $kontak]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Kontak $kontak
     * @return \Illuminate\Http\Response
     */
    public function edit(Kontak $kontak)
    {
        return view('kontak.edit', ['data' => $kontak], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Kontak $kontak
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Kontak $kontak)
    {

        $rules = [
            "name" => [
                "required"
            ],
            "icon" => [
               "sometimes", "nullable", "image"
            ],
            "html" => [
                "required",
            ],
        ];
        $this->validate($request, $rules);

        $db = $kontak;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kontak berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kontak gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Kontak $kontak
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kontak $kontak)
    {
        Cache::forget("kontak");
        $save = $kontak->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kontak berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kontak gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Kontak $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        Cache::forget("kontak");
        if ($request->hasFile('icon')) {
            $filename = Str::random() . "." . $request->file('icon')->getClientOriginalExtension();
            Storage::putFileAs('uploads', $request->file('icon'), $filename);
            $db->icon = $filename;
        }
        $db->name = $request->name;
        $db->html = $request->html;

        return $db->save();
    }
}
