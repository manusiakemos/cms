<?php

namespace App\Http\Controllers;

use App\Models\Widget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class WidgetController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Widget::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Widget berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Widget gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Widget::query();
        /*if ($request->has("widget_status") && $request->widget_status != null) {
            $data = $data->where("_widget.widget_status","=", $request->widget_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Widget $data) {
                    return view("widget.actions_widget", ['data' => $data]);
                })
                ->addColumn('checked', function (Widget $data) {
                    return view("widget.checked_widget", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('widget.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('widget.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "name" => [
                "required"
            ],
//            "icon" => [
//                "required"
//            ],
            "html" => [
                "required"
            ],
//            "fixed" => [
//                "required"
//            ],
        ];
        $this->validate($request, $rules);

        $db = new Widget;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Widget berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Widget gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Widget $widget
     * @return \Illuminate\Http\Response
     */
    public function show(Widget $widget)
    {
        return view('widget.show', ['data' => $widget]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Widget $widget
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget $widget)
    {
        return view('widget.edit', ['data' => $widget], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Widget $widget
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Widget $widget)
    {

        $rules = [
            "name" => [
                "required"
            ],
//            "icon" => [
//                "required"
//            ],
            "html" => [
                "required"
            ],
//            "fixed" => [
//                "required"
//            ],
        ];
        $this->validate($request, $rules);

        $db = $widget;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Widget berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Widget gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Widget $widget
     * @return \Illuminate\Http\Response
     */
    public function destroy(Widget $widget)
    {
        $save = $widget->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Widget berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Widget gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Widget $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->name = $request->name;
        if ($request->hasFile('icon')) {
            $filename = Str::random() . "." . $request->file('icon')->getClientOriginalExtension();
            Storage::putFileAs('uploads', $request->file('icon'), $filename);
            $db->icon = $filename;
        }
        $db->html = $request->html;
        $db->fixed = $request->fixed ?? 1;
        $db->css_cdn = $request->css_cdn;
        $db->js_cdn = $request->js_cdn;

        return $db->save();
    }
}
