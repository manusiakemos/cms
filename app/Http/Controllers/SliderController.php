<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SliderController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget("sliders");
        $data = collect($request->data);
        $save = Slider::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Slider berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Slider gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Slider::query();
        /*if ($request->has("slider_status") && $request->slider_status != null) {
            $data = $data->where("_slider.slider_status","=", $request->slider_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Slider $data) {
                    return view("slider.actions_slider", ['data' => $data]);
                })
                ->addColumn('checked', function (Slider $data) {
                    return view("slider.checked_slider", ['data' => $data]);
                })
                ->editColumn('aktif', function (Slider $data){
                    return boolean_text($data->aktif, 'Aktif', 'Nonaktif');
                })
                ->editColumn('gambar', function (Slider $data){
                    return "<a href='".asset('/uploads/slide/'.$data->gambar)."' target='_blank'>Lihat Gambar</a>";
                })
                ->rawColumns(['action', 'checked', 'gambar'])
                ->toJson();
        }

        return view('slider.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slider.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Slider;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Slider berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Slider gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('slider.show', ['data' => $slider]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('slider.edit', ['data' => $slider], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Slider $slider
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Slider $slider)
    {

        $rules = [
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $slider;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Slider berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Slider gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Slider $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        Cache::forget("sliders");
        $save = $slider->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Slider berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Slider gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Slider $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        Cache::forget("sliders");
        ini_set('memory_limit', '512M');
        ini_set('max_file_uploads', '32M');
        ini_set('post_max_size', '32M');
        $db->nama = $request->nama;
        $db->isi = $request->isi;
        $db->aktif = $request->aktif;
        if ($request->hasFile('gambar')) {
            $db->gambar = resizeGambar($request->file('gambar'), 'uploads/slide/', 2000, 1336);
        }

        return $db->save();
    }
}
