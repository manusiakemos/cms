<?php

namespace App\Http\Controllers;

use App\Models\KategoriBerita;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class KategoriBeritaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = KategoriBerita::whereIn("kategori_id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'KategoriBerita berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'KategoriBerita gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = KategoriBerita::query();
        /*if ($request->has("kategoriberita_status") && $request->kategoriberita_status != null) {
            $data = $data->where("_kategoriberita.kategoriberita_status","=", $request->kategoriberita_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (KategoriBerita $data) {
                    return view("kategoriberita.actions_kategoriberita", ['data' => $data]);
                })
                ->addColumn('checked', function (KategoriBerita $data) {
                    return view("kategoriberita.checked_kategoriberita", ['data' => $data]);
                })
                ->editColumn('aktif', function (KategoriBerita $data) {
                    return boolean_text($data->aktif, 'aktif', 'nonaktif');
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('kategoriberita.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategoriberita.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "nama" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new KategoriBerita;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'KategoriBerita berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'KategoriBerita gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\KategoriBerita $kategoriberita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $db = KategoriBerita::findOrFail($id);
        return view('kategoriberita.show', ['data' => $db]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param KategoriBerita $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $db = KategoriBerita::where("kategori_id", $id)->firstOrFail();
        return view('kategoriberita.edit', ['data' => $db], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\KategoriBerita $kategoriberita
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $rules = [
            "nama" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = KategoriBerita::find($id);
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'KategoriBerita berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'KategoriBerita gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\KategoriBerita $kategoriberita
     * @return \Illuminate\Http\Response
     */
    public function destroy($kategoriberita)
    {
        $save = KategoriBerita::destroy($kategoriberita);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'KategoriBerita berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'KategoriBerita gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\KategoriBerita $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->nama = $request->nama;
        $db->aktif = $request->aktif;

        return $db->save();
    }
}
