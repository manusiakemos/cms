<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VotingController extends Controller
{
    public function index()
    {
        $file = file_get_contents(database_path("json/voting.json"));
        $collect = collect(json_decode($file));
        $total = $collect->sum('value');
        $collect->mapToGroups(function ($item, $key) use ($total){
            return $total ? [$item->percent = number_format($item->value / $total * 100, 2)] : [0];
        });
        $resData = [
            'total' => $total,
            'data' => $collect->all()
        ];
        return responseJson('Terimakasih sudah mengikuti poling',$resData);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'voted' => 'required',
            'captcha' => 'required|captcha'
        ]);
        $file = file_get_contents(database_path("json/voting.json"));
        $data = json_decode($file);
        $data->{$request->voted}->value = $data->{$request->voted}->value + 1;
        file_put_contents(database_path("json/voting.json"), json_encode($data));
        $file = file_get_contents(database_path("json/voting.json"));
        $collect = collect(json_decode($file));
        $total = $collect->sum('value');
        $collect->mapToGroups(function ($item, $key) use ($total){
            return [$item->percent = number_format($item->value / $total * 100, 2)];
        });
        $resData = [
            'total' => $total,
            'data' => $collect->all()
        ];
        return responseJson('Terimakasih sudah mengikuti poling',$resData);
    }
}
