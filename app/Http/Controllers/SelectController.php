<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SelectController extends Controller
{
    public function __invoke(Request $request, $case)
    {
        switch ($case){
            case 'boolean':
                return [
                    ['value' => 1, 'text' =>'Ya'],
                    ['value' => 0, 'text' =>'Tidak'],
                ];
                break;
            case 'boolean_active':
                return [
                    ['value' => 1, 'text' =>'Aktif'],
                    ['value' => 0, 'text' =>'Nonaktif'],
                ];
                break;
        }
    }
}
