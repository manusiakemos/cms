<?php

namespace App\Http\Controllers;

use App\Models\PropemPengusul;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class PropemPengusulController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
        * @param Request $request
        * @return \Illuminate\Http\JsonResponse
        * hapus semua
        */
       public function bulkDelete(Request $request)
       {
           $data = collect($request->data);
           $save = PropemPengusul::whereIn("id", $data->pluck("id")->unique())->delete();
           return $save
               ? response()->json(['status' => true, 'text' => 'success', 'message' => 'PropemPengusul berhasil dihapus'])
               : response()->json(['status' => false, 'text' => 'error', 'message' => 'PropemPengusul gagal dihapus'], 500);
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = PropemPengusul::query();
        /*if ($request->has("propempengusul_status") && $request->propempengusul_status != null) {
            $data = $data->where("_propempengusul.propempengusul_status","=", $request->propempengusul_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (PropemPengusul $data) {
                    return view("propempengusul.actions_propempengusul", ['data' => $data]);
                })
                ->addColumn('checked', function (PropemPengusul $data) {
                    return view("propempengusul.checked_propempengusul", ['data' => $data]);
                })
                ->rawColumns(['action','checked'])
                ->toJson();
        }

        return view('propempengusul.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('propempengusul.create',['modal_title'=>'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
    "nama" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = new PropemPengusul;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'PropemPengusul berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'PropemPengusul gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropemPengusul $propempengusul
     * @return \Illuminate\Http\Response
     */
    public function show(PropemPengusul $propempengusul)
    {
        return view('propempengusul.show', ['data' => $propempengusul]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropemPengusul $propempengusul
     * @return \Illuminate\Http\Response
     */
    public function edit(PropemPengusul $propempengusul)
    {
        return view('propempengusul.edit', ['data' => $propempengusul], ['modal_title'=>'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\PropemPengusul $propempengusul
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, PropemPengusul $propempengusul)
    {

       $rules = [
    "nama" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = $propempengusul;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'PropemPengusul berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'PropemPengusul gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropemPengusul $propempengusul
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropemPengusul $propempengusul)
    {
        $save = $propempengusul->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'PropemPengusul berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'PropemPengusul gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\PropemPengusul $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->nama = $request->nama;
    
        return $db->save();
    }
}
