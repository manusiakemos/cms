<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ArtisanController extends Controller
{
    /**
     * @param $case
     */
    public function __invoke($case)
    {
        // TODO: Implement __invoke() method.

        switch($case){
            case "migrate":
                Artisan::call("migrate");
                break;
            case "migrate-fresh":
                Artisan::call("migrate:fresh --seed");
                break;
            case "seed":
                Artisan::call("db:seed");
                break;
            case "storage":
                Artisan::call("storage:link");
                break;
            case "startapp":
                Artisan::call("migrate");
                Artisan::call("db:seed");
                Artisan::call("storage:link");
                break;
        }
    }
}
