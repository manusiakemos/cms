<?php

namespace App\Http\Controllers;

use App\Http\Resources\IntegrasiResources;
use App\Models\Berita;
use App\Models\Halaman;
use App\Models\Katalog;
use App\Models\Kategori;
use App\Models\Regulasi;
use Illuminate\Support\Facades\Http;

class JdihController extends Controller
{
    public function __invoke($methodName)
    {
        return $this->{$methodName}();
    }

    public function regulasi()
    {
        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/regulasi')->json();
//        return $response;
//        Cache::put('cache_regulasi', $response);
//        $cache = Cache::get('cache_regulasi');
        $kategori_data = Kategori::all();
        $insert = [];
        foreach ($response as $value) {
            $kategori = $value['reg_kategori'];
            $findKategoriId = $kategori_data->where("kategori_nama", ucwords($kategori))->first();
            $insert[] = [
                "reg_id" => $value['reg_id'],
                "kategori_id" => $findKategoriId->kategori_id,
                "reg_judul" => $value['reg_judul'],
                "reg_nomor" => $value['reg_nomor'],
                "reg_filename" => $value['reg_filename'],
                "reg_tahun" => $value['reg_tahun'],
                "reg_tanggal" => $value['reg_tanggal'],
            ];
        }

        Regulasi::truncate();
        if (Regulasi::insert($insert)) {
            return responseJson('success');
        }

        return responseJson('error', [], 'error');
    }

    public function kategori()
    {
//        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/kategori')->json();
//        $response = collect($response)->toArray();
//        foreach ($response as $value) {
//            $insert[] = [
//                "kategori_id" => $value["id"],
//                "nama" => $value["nama"],
//                "aktif" => 1,
//            ];
//        }
//
//        KategoriBerita::insert($insert);
    }

    public function slide()
    {
//        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/slide')->json();
//        $response = collect($response)->toArray();
//        Slider::insert($response);
    }

    public function halaman()
    {
        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/halaman')->json();
        $response = collect($response)->toArray();

        Halaman::insert($response);
    }

    public function gallery()
    {
//        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/galeri')->json();
//        $response = collect($response)->toArray();
//
//        foreach ($response as $value) {
//            $ins[] = [
//                "id" => $value["gal_id"],
//                "judul" => $value["gal_title"],
//                "keterangan" => $value["gal_caption"],
//                "slug" => $value["gal_slug"],
//                "filename" => $value["gal_filename"],
//            ];
//        }
//
//        Gallery::insert($ins);
    }

    public function berita()
    {
//        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/berita')->json();
//        $response = collect($response)->toArray();
//        return $response;
//        Berita::insert($response);
    }

    public function katalog()
    {
        $response = Http::post('https://jdih.tabalongkab.go.id/api/copy/katalog')->json();
        $response = collect($response)->toArray();
//        return $response;
        $kategori_data = Kategori::all();
        $insert = [];
        foreach ($response as $value) {
            $kategori = $value['kategori'];
            $findKategoriId = $kategori_data->where("kategori_nama", ucwords($kategori))->first();
            $insert[] = [
                "kategori_id" => $findKategoriId ? $findKategoriId->kategori_id : 0,
                "katalog_id" => $value['katalog_id'],
                "judul" => $value['judul'],
                "nomor" => $value['nomor'],
                "filename" => $value['filename'],
                "tahun" => $value['tahun'],
//                "tanggal" => $value['tanggal'],
            ];
        }

        Katalog::truncate();
        if (Katalog::insert($insert)) {
            return responseJson('success');
        }

        return responseJson('error', [], 'error');
    }

    public function integrasi()
    {
        $data = Regulasi::joinKategori()->get();
        return IntegrasiResources::collection($data);
    }

}
