<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class GalleryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget("gallery");
        $data = collect($request->data);
        $save = Gallery::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Gallery berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Gallery gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Gallery::query();
        /*if ($request->has("gallery_status") && $request->gallery_status != null) {
            $data = $data->where("_gallery.gallery_status","=", $request->gallery_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Gallery $data) {
                    return view("gallery.actions_gallery", ['data' => $data]);
                })
                ->addColumn('checked', function (Gallery $data) {
                    return view("gallery.checked_gallery", ['data' => $data]);
                })
                ->addColumn('checked', function (Gallery $data) {
                    return view("gallery.checked_gallery", ['data' => $data]);
                })
                ->editColumn('filename', function (Gallery $data) {
                    return "<a target='_blank' href='".asset('uploads/galeri/'.$data->filename)."'>Lihat Gambar</a>";
                })

                ->rawColumns(['action', 'checked', 'filename'])
                ->toJson();
        }

        return view('gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "judul" => [
                "required"
            ],
            "keterangan" => [
                "required"
            ],
            "filename" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Gallery;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Gallery berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Gallery gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        return view('gallery.show', ['data' => $gallery]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        return view('gallery.edit', ['data' => $gallery], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Gallery $gallery
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Gallery $gallery)
    {

        $rules = [
            "judul" => [
                "required"
            ],
            "keterangan" => [
                "required"
            ],
            "filename" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $gallery;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Gallery berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Gallery gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        Cache::forget("gallery");
        $save = $gallery->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Gallery berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Gallery gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Gallery $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_file_uploads', '32M');
        ini_set('post_max_size', '32M');
        Cache::forget("gallery");
        $db->judul = $request->judul;
        $db->keterangan = $request->keterangan;
        $db->slug = Str::slug($request->judul);
        if ($request->hasFile('filename')) {
            $directory = storage_path('/app/uploads/galeri/');
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            $filename = resizeGambar($request->file('filename'), 'uploads/galeri/');
            $db->filename = $filename;
        }

        return $db->save();
    }
}
