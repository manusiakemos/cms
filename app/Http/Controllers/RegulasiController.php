<?php

namespace App\Http\Controllers;

use App\Models\Regulasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class RegulasiController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Regulasi::whereIn("reg_id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Regulasi berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Regulasi gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Regulasi::JoinKategori();
        /*if ($request->has("regulasi_status") && $request->regulasi_status != null) {
            $data = $data->where("_regulasi.regulasi_status","=", $request->regulasi_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Regulasi $data) {
                    return view("regulasi.actions_regulasi", ['data' => $data]);
                })
                ->addColumn('checked', function (Regulasi $data) {
                    return view("regulasi.checked_regulasi", ['data' => $data]);
                })
                ->editColumn('reg_filename', function (Regulasi $data){
                    return "<a href='".asset('uploads/pdf/'.$data->reg_filename)."'>Lihat File</a>";
                })
                ->rawColumns(['action', 'checked', 'reg_filename'])
                ->toJson();
        }

        return view('regulasi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('regulasi.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "kategori_id" => [
                "required"
            ],
            "reg_nomor" => [
                "required"
            ],
            "reg_filename" => [
                "required"
            ],
            "reg_tahun" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Regulasi;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Regulasi berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Regulasi gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Regulasi $regulasi
     * @return \Illuminate\Http\Response
     */
    public function show(Regulasi $regulasi)
    {
        return view('regulasi.show', ['data' => $regulasi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Regulasi $regulasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Regulasi $regulasi)
    {
        return view('regulasi.edit', ['data' => $regulasi], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Regulasi $regulasi
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Regulasi $regulasi)
    {

        $rules = [
            "kategori_id" => [
                "required"
            ],
            "reg_nomor" => [
                "required"
            ],
            "reg_filename" => [
                "required"
            ],
            "reg_tahun" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $regulasi;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Regulasi berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Regulasi gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Regulasi $regulasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Regulasi $regulasi)
    {
        $save = $regulasi->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Regulasi berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Regulasi gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Regulasi $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->kategori_id = $request->kategori_id;
        $db->reg_judul = $request->reg_judul;
        $db->reg_nomor = $request->reg_nomor;
        if ($request->hasFile('reg_filename')) {
            $filename = Str::random() . "." . $request->file('reg_filename')->getClientOriginalExtension();
            Storage::putFileAs('uploads/pdf/', $request->file('reg_filename'), $filename);
            $db->reg_filename = $filename;
        }
        $db->reg_tahun = $request->reg_tahun;
        $db->reg_tanggal = $request->reg_tanggal;

        return $db->save();
    }
}
