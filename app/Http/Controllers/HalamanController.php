<?php

namespace App\Http\Controllers;

use App\Models\Halaman;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class HalamanController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {
        return Halaman::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Halaman::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Halaman berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Halaman gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Halaman::query();
        /*if ($request->has("halaman_status") && $request->halaman_status != null) {
            $data = $data->where("_halaman.halaman_status","=", $request->halaman_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Halaman $data) {
                    return view("halaman.actions_halaman", ['data' => $data]);
                })
                ->addColumn('checked', function (Halaman $data) {
                    return view("halaman.checked_halaman", ['data' => $data]);
                })
                ->editColumn('aktif', function (Halaman $data) {
                    return boolean_text($data->aktif);
                })
                ->editColumn('url', function (Halaman $data) {
                    return "<a href='".url('berita/'.$data->url)."'>{$data->url}</a>";
                })
                ->rawColumns(['action', 'checked', 'url'])
                ->toJson();
        }

        return view('halaman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('halaman.create', ['modal_title' => 'Tambah Halaman']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "judul" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
            "isi" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Halaman;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Halaman berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Halaman gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Halaman $halaman
     * @return \Illuminate\Http\Response
     */
    public function show(Halaman $halaman)
    {
        return view('halaman.show', ['data' => $halaman]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Halaman $halaman
     * @return \Illuminate\Http\Response
     */
    public function edit(Halaman $halaman)
    {
        return view('halaman.edit', ['data' => $halaman], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Halaman $halaman
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Halaman $halaman)
    {

        $rules = [
            "judul" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
            "isi" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $halaman;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Halaman berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Halaman gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Halaman $halaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Halaman $halaman)
    {
        $save = $halaman->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Halaman berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Halaman gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Halaman $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->judul = $request->judul;
        $db->custom = 0; //$request->custom;
        $db->url = Str::slug($request->judul);//$request->url;
        $db->aktif = $request->aktif;
        $db->isi = $request->isi;

        return $db->save();
    }
}
