<?php

namespace App\Http\Controllers;

use App\Models\LinkTerkait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yajra\DataTables\DataTables;

class LinkTerkaitController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget("linkterkait");
        $data = collect($request->data);
        $save = LinkTerkait::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'LinkTerkait berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'LinkTerkait gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = LinkTerkait::query();
        /*if ($request->has("linkterkait_status") && $request->linkterkait_status != null) {
            $data = $data->where("_linkterkait.linkterkait_status","=", $request->linkterkait_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (LinkTerkait $data) {
                    return view("linkterkait.actions_linkterkait", ['data' => $data]);
                })
                ->addColumn('checked', function (LinkTerkait $data) {
                    return view("linkterkait.checked_linkterkait", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('linkterkait.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('linkterkait.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "name" => [
                "required"
            ],
            "url" => [
                "required"
            ],
            "gambar" => [
                "exclude_unless:gambar, image"
            ],
        ];
        $this->validate($request, $rules);

        $db = new LinkTerkait;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'LinkTerkait berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'LinkTerkait gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\LinkTerkait $linkterkait
     * @return \Illuminate\Http\Response
     */
    public function show(LinkTerkait $linkterkait)
    {
        return view('linkterkait.show', ['data' => $linkterkait]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\LinkTerkait $linkterkait
     * @return \Illuminate\Http\Response
     */
    public function edit(LinkTerkait $linkterkait)
    {
        return view('linkterkait.edit', ['data' => $linkterkait], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\LinkTerkait $linkterkait
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, LinkTerkait $linkterkait)
    {

        $rules = [
            "name" => [
                "required"
            ],
            "url" => [
                "required"
            ],
            "gambar" => [
                "exclude_unless:gambar, image"
            ],
        ];
        $this->validate($request, $rules);

        $db = $linkterkait;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'LinkTerkait berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'LinkTerkait gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\LinkTerkait $linkterkait
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinkTerkait $linkterkait)
    {
        Cache::forget("linkterkait");

        $save = $linkterkait->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'LinkTerkait berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'LinkTerkait gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\LinkTerkait $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        Cache::forget("linkterkait");

        $db->name = $request->name;
        $db->url = $request->url;
        if ($request->hasFile('gambar')) {
            $filename = resizeAspectGambar($request->file('gambar'), 'uploads/linkterkait/', null, 70);
            $db->gambar = $filename;
        }

        return $db->save();
    }
}
