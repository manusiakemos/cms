<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class KategoriController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
        * @param Request $request
        * @return \Illuminate\Http\JsonResponse
        * hapus semua
        */
       public function bulkDelete(Request $request)
       {
           $data = collect($request->data);
           $save = Kategori::whereIn("kategori_id", $data->pluck("id")->unique())->delete();
           return $save
               ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kategori berhasil dihapus'])
               : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kategori gagal dihapus'], 500);
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Kategori::query();
        /*if ($request->has("kategori_status") && $request->kategori_status != null) {
            $data = $data->where("_kategori.kategori_status","=", $request->kategori_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Kategori $data) {
                    return view("kategori.actions_kategori", ['data' => $data]);
                })
                ->editColumn('kategori_aktif', function (Kategori $data) {
                    return boolean_text($data->kategori_aktif, 'aktif', 'nonaktif');
                })
                ->addColumn('checked', function (Kategori $data) {
                    return view("kategori.checked_kategori", ['data' => $data]);
                })
                ->rawColumns(['action','checked'])
                ->toJson();
        }

        return view('kategori.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create',['modal_title'=>'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
    "kategori_nama" => [
    "required"
    ],
    "kategori_aktif" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = new Kategori;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kategori berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kategori gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kategori $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        return view('kategori.show', ['data' => $kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kategori $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('kategori.edit', ['data' => $kategori], ['modal_title'=>'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Kategori $kategori
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Kategori $kategori)
    {

       $rules = [
    "kategori_nama" => [
    "required"
    ],
    "kategori_aktif" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = $kategori;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kategori berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kategori gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kategori $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        $save = $kategori->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Kategori berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Kategori gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Kategori $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->kategori_nama = $request->kategori_nama;
                $db->kategori_aktif = $request->kategori_aktif;

        return $db->save();
    }
}
