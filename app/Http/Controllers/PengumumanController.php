<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yajra\DataTables\DataTables;

class PengumumanController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget("pengumuman");
        $data = collect($request->data);
        $save = Pengumuman::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Pengumuman berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Pengumuman gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Pengumuman::query();
        /*if ($request->has("pengumuman_status") && $request->pengumuman_status != null) {
            $data = $data->where("_pengumuman.pengumuman_status","=", $request->pengumuman_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->editColumn('aktif', function (Pengumuman $data) {
                    return boolean_text($data->aktif);
                })
                ->addColumn('action', function (Pengumuman $data) {
                    return view("pengumuman.actions_pengumuman", ['data' => $data]);
                })
                ->addColumn('checked', function (Pengumuman $data) {
                    return view("pengumuman.checked_pengumuman", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('pengumuman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengumuman.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "judul" => [
                "required"
            ],
            "tanggal" => [
                "required"
            ],
            "isi" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Pengumuman;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Pengumuman berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Pengumuman gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Pengumuman $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function show(Pengumuman $pengumuman)
    {
        return view('pengumuman.show', ['data' => $pengumuman]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Pengumuman $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengumuman $pengumuman)
    {
        return view('pengumuman.edit', ['data' => $pengumuman], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pengumuman $pengumuman
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Pengumuman $pengumuman)
    {

        $rules = [
            "judul" => [
                "required"
            ],
            "tanggal" => [
                "required"
            ],
            "isi" => [
                "required"
            ],
            "aktif" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $pengumuman;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Pengumuman berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Pengumuman gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Pengumuman $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengumuman $pengumuman)
    {
        Cache::forget("pengumuman");
        $save = $pengumuman->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Pengumuman berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Pengumuman gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Pengumuman $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        Cache::forget("pengumuman");
        $db->judul = $request->judul;
        $db->tanggal = $request->tanggal;
        $db->isi = $request->isi;
        $db->aktif = $request->aktif;

        return $db->save();
    }
}
