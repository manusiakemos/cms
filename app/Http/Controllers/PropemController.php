<?php

namespace App\Http\Controllers;

use App\Models\Propem;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PropemController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        $data = collect($request->data);
        $save = Propem::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Propem berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Propem gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Propem::joinData();
        /*if ($request->has("propem_status") && $request->propem_status != null) {
            $data = $data->where("_propem.propem_status","=", $request->propem_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Propem $data) {
                    return view("propem.actions_propem", ['data' => $data]);
                })
                ->addColumn('checked', function (Propem $data) {
                    return view("propem.checked_propem", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked'])
                ->toJson();
        }

        return view('propem.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('propem.create', ['modal_title' => 'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "pengusul_id" => [
                "required"
            ],
            "judul" => [
                "required"
            ],
            "no" => [
                "required"
            ],
            "tanggal" => [
                "required"
            ],
            "tahun" => [
                "required"
            ],
//            "file" => [
//                "required"
//            ],
        ];
        $this->validate($request, $rules);

        $db = new Propem;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Propem berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Propem gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Propem $propem
     * @return \Illuminate\Http\Response
     */
    public function show(Propem $propem)
    {
        return view('propem.show', ['data' => $propem]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Propem $propem
     * @return \Illuminate\Http\Response
     */
    public function edit(Propem $propem)
    {
        return view('propem.edit', ['data' => $propem], ['modal_title' => 'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Propem $propem
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Propem $propem)
    {

        $rules = [
            "pengusul_id" => [
                "required"
            ],
            "judul" => [
                "required"
            ],
            "no" => [
                "required"
            ],
            "tanggal" => [
                "required"
            ],
            "tahun" => [
                "required"
            ],
//            "file" => [
//                "required"
//            ],
        ];
        $this->validate($request, $rules);

        $db = $propem;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Propem berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Propem gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Propem $propem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Propem $propem)
    {
        $save = $propem->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Propem berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Propem gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Propem $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->pengusul_id = $request->pengusul_id;
        $db->judul = $request->judul;
        $db->no = $request->no;
        $db->tanggal = $request->tanggal;
        $db->tahun = $request->tahun;

        return $db->save();
    }
}
