<?php

namespace App\Http\Controllers;

use App\Models\Saran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class SaranController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
        * @param Request $request
        * @return \Illuminate\Http\JsonResponse
        * hapus semua
        */
       public function bulkDelete(Request $request)
       {
           $data = collect($request->data);
           $save = Saran::whereIn("saran_id", $data->pluck("id")->unique())->delete();
           return $save
               ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Saran berhasil dihapus'])
               : response()->json(['status' => false, 'text' => 'error', 'message' => 'Saran gagal dihapus'], 500);
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Saran::query();
        /*if ($request->has("saran_status") && $request->saran_status != null) {
            $data = $data->where("_saran.saran_status","=", $request->saran_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Saran $data) {
                    return view("saran.actions_saran", ['data' => $data]);
                })
                ->addColumn('checked', function (Saran $data) {
                    return view("saran.checked_saran", ['data' => $data]);
                })
                ->rawColumns(['action','checked'])
                ->toJson();
        }

        return view('saran.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('saran.create',['modal_title'=>'Tambah Data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
    "saran_content" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = new Saran;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Saran berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Saran gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Saran $saran
     * @return \Illuminate\Http\Response
     */
    public function show(Saran $saran)
    {
        return view('saran.show', ['data' => $saran]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Saran $saran
     * @return \Illuminate\Http\Response
     */
    public function edit(Saran $saran)
    {
        return view('saran.edit', ['data' => $saran], ['modal_title'=>'Edit Data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Saran $saran
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Saran $saran)
    {

       $rules = [
    "saran_content" => [
    "required"
    ],
];
$this->validate($request, $rules);

        $db = $saran;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Saran berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Saran gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Saran $saran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Saran $saran)
    {
        $save = $saran->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Saran berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Saran gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Saran $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        $db->saran_content = $request->saran_content;

        return $db->save();
    }
}
