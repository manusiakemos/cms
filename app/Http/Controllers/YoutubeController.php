<?php

namespace App\Http\Controllers;

use App\Models\Youtube;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Yajra\DataTables\DataTables;

class YoutubeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|super-admin'])->except('api');
    }

    public function api(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * hapus semua
     */
    public function bulkDelete(Request $request)
    {
        Cache::forget('youtube');
        $data = collect($request->data);
        $save = Youtube::whereIn("id", $data->pluck("id")->unique())->delete();
        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Youtube berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Youtube gagal dihapus'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $data = Youtube::query();
        /*if ($request->has("youtube_status") && $request->youtube_status != null) {
            $data = $data->where("_youtube.youtube_status","=", $request->youtube_status);
        }*/
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function (Youtube $data) {
                    return view("youtube.actions_youtube", ['data' => $data]);
                })
                ->addColumn('checked', function (Youtube $data) {
                    return view("youtube.checked_youtube", ['data' => $data]);
                })
                ->rawColumns(['action', 'checked', 'embed'])
                ->toJson();
        }

        return view('youtube.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('youtube.create', ['modal_title' => 'Tambah']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "embed" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = new Youtube;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Youtube berhasil ditambahkan'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Youtube gagal ditambahkan'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Youtube $youtube
     * @return \Illuminate\Http\Response
     */
    public function show(Youtube $youtube)
    {
        return view('youtube.show', ['data' => $youtube]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Youtube $youtube
     * @return \Illuminate\Http\Response
     */
    public function edit(Youtube $youtube)
    {
        return view('youtube.edit', ['data' => $youtube], ['modal_title' => 'Edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Youtube $youtube
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Youtube $youtube)
    {

        $rules = [
            "embed" => [
                "required"
            ],
        ];
        $this->validate($request, $rules);

        $db = $youtube;
        $save = $this->handleRequest($request, $db);

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Youtube berhasil diupdate'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Youtube gagal diupdate'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Youtube $youtube
     * @return \Illuminate\Http\Response
     */
    public function destroy(Youtube $youtube)
    {
        Cache::forget('youtube');
        $save = $youtube->delete();

        return $save
            ? response()->json(['status' => true, 'text' => 'success', 'message' => 'Youtube berhasil dihapus'])
            : response()->json(['status' => false, 'text' => 'error', 'message' => 'Youtube gagal dihapus'], 500);
    }

    /**
     * @param $request
     * @param \App\Models\Youtube $db
     * @return mixed
     */
    private function handleRequest(Request $request, $db)
    {
        Cache::forget('youtube');

        $db->name = $request->name;
        $db->embed = $request->embed;
        $db->caption = $request->caption;

        return $db->save();
    }
}
