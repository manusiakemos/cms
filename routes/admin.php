<?php

use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;



if(config('crud.mode') == 'mpa'){
    Route::get('/home','HomeController@index')->name('home');

    Route::get('/myprofile',  'Admin\ProfileController@showProfilePage')->name('profile');
    Route::put('/myprofile', 'Admin\ProfileController@updateProfile');
    Route::get('/password',  'Admin\ProfileController@showPasswordPage')->name('password');
    Route::put('/password',  'Admin\ProfileController@changePassword');


    Route::post('usermanagement/bulkdelete', 'Admin\UserManagementController@bulkDelete')->name('usermanagement.bulkdelete');
    Route::post('usermanagement/api', 'Admin\UserManagementController@api')->name('usermanagement.api');
    Route::resource('usermanagement', 'Admin\UserManagementController');

    Route::middleware(['auth'])->group(function(){


        Route::post('youtube/bulkdelete', 'YoutubeController@bulkDelete')->name('youtube.bulkdelete');
        Route::post('youtube/api','YoutubeController@api')->name('youtube.api');
        Route::resource('youtube', 'YoutubeController');

        //generated routes controller middleware
        Route::post('slider/bulkdelete', 'SliderController@bulkDelete')->name('slider.bulkdelete');
//    Route::post('slider/api', 'SliderController@api')->name('slider.api');
        Route::resource('slider', 'SliderController');

        Route::post('regulasi/bulkdelete', 'RegulasiController@bulkDelete')->name('regulasi.bulkdelete');
//Route::post('regulasi/api', 'RegulasiController@api')->name('regulasi.api');
        Route::resource('regulasi', 'RegulasiController');

        Route::post('kategori/bulkdelete', 'KategoriController@bulkDelete')->name('kategori.bulkdelete');
//Route::post('kategori/api', 'KategoriController@api')->name('kategori.api');
        Route::resource('kategori', 'KategoriController');

        Route::post('saran/bulkdelete', 'SaranController@bulkDelete')->name('saran.bulkdelete');
        Route::resource('saran', 'SaranController');

        Route::post('katalog/bulkdelete', 'KatalogController@bulkDelete')->name('katalog.bulkdelete');
        Route::resource('katalog', 'KatalogController');

        Route::post('halaman/bulkdelete', 'HalamanController@bulkDelete')->name('halaman.bulkdelete');
        Route::post('halaman/api', 'HalamanController@api')->name('halaman.api');
        Route::resource('halaman', 'HalamanController');

        Route::post('gallery/bulkdelete', 'GalleryController@bulkDelete')->name('gallery.bulkdelete');
        Route::resource('gallery', 'GalleryController');

        Route::post('berita/bulkdelete', 'BeritaController@bulkDelete')->name('berita.bulkdelete');
        Route::resource('berita', 'BeritaController');

        Route::post('kategoriberita/bulkdelete', 'KategoriBeritaController@bulkDelete')->name('kategoriberita.bulkdelete');
        Route::resource('kategoriberita', 'KategoriBeritaController');

        Route::post('linkterkait/bulkdelete', 'LinkTerkaitController@bulkDelete')->name('linkterkait.bulkdelete');
        Route::resource('linkterkait', 'LinkTerkaitController');

        Route::post('menu/api', 'MenuController@api')->name('menu.api');
        Route::post('menu/save', 'MenuController@save')->name('menu.save');
        Route::resource('menu', 'MenuController')->only(['destroy', 'index']);

        Route::post('pengumuman/api', 'PengumumanController@api')->name('pengumuman.api');
        Route::post('pengumuman/bulkdelete', 'PengumumanController@bulkDelete')->name('pengumuman.bulkdelete');
        Route::resource('pengumuman', 'PengumumanController');

        Route::post('propem/bulkdelete', 'PropemController@bulkDelete')->name('propem.bulkdelete');
        Route::post('propem/api', 'PropemController@api')->name('propem.api');
        Route::resource('propem', 'PropemController');

        Route::post('propempengusul/bulkdelete', 'PropemPengusulController@bulkDelete')->name('propempengusul.bulkdelete');
        Route::post('propempengusul/api', 'PropemPengusulController@api')->name('propempengusul.api');
        Route::resource('propempengusul', 'PropemPengusulController');

        Route::post('setting/bulkdelete', 'SettingController@bulkDelete')->name('setting.bulkdelete');
        Route::post('setting/api', 'SettingController@api')->name('setting.api');
        Route::resource('setting', 'SettingController');

        Route::post('settinggroup/bulkdelete', 'SettingGroupController@bulkDelete')->name('settinggroup.bulkdelete');
        Route::post('settinggroup/api', 'SettingGroupController@api')->name('settinggroup.api');
        Route::resource('settinggroup', 'SettingGroupController');

        Route::post('kontak/bulkdelete', 'KontakController@bulkDelete')->name('kontak.bulkdelete');
        Route::post('kontak/api', 'KontakController@api')->name('kontak.api');
        Route::resource('kontak', 'KontakController');

        Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth']], function () {
            Lfm::routes();
        });

        Route::get('/css-editor', 'CssEditorController@show')->name('css.show');
        Route::post('/css-editor', 'CssEditorController@save');

        Route::get('instagram', function (){
            $profile = \Dymantic\InstagramFeed\Profile::where('username', config('instagram-feed.username'))->first();

            return view('instagram.auth', ['instagram_auth_url' => $profile->getInstagramAuthUrl()]);
        });
    });

}else{
    Route::get('/', 'Admin\AdminController');
}

if(config('crud.use_login') == true){
    Route::get('/login', 'Admin\AuthController@showLoginForm')->name('login');
    Route::post('/login', 'Admin\AuthController@login');
    Route::post('/logout', 'Admin\AuthController@logout')->name('logout');
}
