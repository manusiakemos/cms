<?php

use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('visitor')->group(function(){
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('halaman/{slug}', 'WebController@viewHalaman')->name('view.halaman');


    Route::get('berita', 'WebController@berita')->name('berita');
    Route::get('view/berita/{slug}', 'WebController@viewBerita')->name('view.berita');

    Route::get('view/pengumuman/{slug}', 'WebController@pengumuman')->name('pengumuman');

    Route::get('voting', 'VotingController@index')->name('voting');
    Route::post('voting', 'VotingController@update');

    Route::get('instagram-auth-success', function (){
        abort('200', 'Auth Instagram Success');
    });

    Route::get('instagram-auth-failure', function (){
        abort('500', 'Auth Instagram Failure');
    });
});
