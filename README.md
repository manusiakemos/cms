<h1 align="center">
  CMS INI DIBUAT DENGAN FRAMEWORK
</h1>

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

file .htaccess (khusus upload di cpanel)

```bash
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /

  # Force SSL
  RewriteCond %{HTTPS} !=on
  RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
  # Remove public folder form URL
  RewriteRule ^(.*)$ public/$1 [L]
</IfModule>
```


# PANDUAN INSTALL BARU



1. paste file di cpanel pada folder public_html

2. Setting database dan app_url di .env

3. hapus folder upload di public

4. edit file config/crud.php edit `start_app => false`

5. jalankan terminal di cpanel

   ```bash
   cd public_html
   
   php artisan migrate:fresh --seed
   
   php artisan key:generate
   
   php artisan storage:link
   
   php artisan optimize:clear
   ```

   

6. setting config/crud.php 

   ```php
   return [
   'enabled' => false,
   'mode' => 'mpa',
   'use_login' => true,
   'start_app' => true
   ];
   ```

7. pastikan .env `app_debug = false` dan `app_env = production`

8. edit jumlah kunjungan di folder `database/json/visitor.json`

9. login ke admin urlnya `domain/admin/login`

    | username    | password    | role        |
    | ----------- | ----------- | ----------- |
    | masuk       | masukhaja   | admin       |
    | super-admin | samasukhaja | super-admin |

10. setting versi php ke 7.4

11. setting max file upload dan memory limit



# PANDUAN UPDATE CMS

1. buat folder backup pada public_html

2. pindahkan folder storage ke backup

3. hapus semua file di public html kecuali folder `backup`, `.env`, `.htaccess`, `php.ini`

4. upload file cms.zip

5. hapus folder storage public_html

6. pindahkan folder storage di backup ke public_html

7. jalankan terminal

   ```bash 
   cd public_html
   
   php artisan migrate
   
   php artisan key:generate
   
   php artisan storage:link
   
   php artisan optimize:clear
   ```
