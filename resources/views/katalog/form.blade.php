<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST">
            @csrf
            @isset($method)
                @if($method == "PUT")
                    @method("PUT")
                @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kategori_id">Kategori</label>
                        <x-select-component id="kategori_id"
                                            name="kategori_id"
                                            error-container="#kategori_id-errors"
                                            case="kategori"
                                            text-value="kategori_id"
                                            text-label="kategori_nama"
                                            :selected="isset($data) ? $data->kategori_id : ''">
                        </x-select-component>
                        <div id="kategori_id-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor</label>
                        <input name="nomor" type="text"
                               data-parsley-errors-container="#nomor-errors"
                               value="<?php echo e(isset($data) ? $data->nomor : ""); ?>"
                               class="form-control" id="nomor">
                        <div id="nomor-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input name="judul" type="text"
                               data-parsley-errors-container="#judul-errors"
                               value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                               class="form-control" id="judul">
                        <div id="judul-errors"></div>
                    </div>
                    {{--<div class="form-group">
                        <div class="custom-file mt-4">
                            <input name="filename" type="file"
                                   class="custom-file-input" id="filename">
                            <label class="custom-file-label" for="filename">File</label>
                        </div>
                    </div>--}}
                    <div class="form-group">
                        <label for="Tahun">tahun</label>
                        <input name="Tahun" type="text"
                               placeholder="YYYY-MM-DD"
                               data-parsley-errors-container="#Tahun-errors"
                               value="<?php echo e(isset($data) ? $data->Tahun : "2020-12-30"); ?>"
                               class="form-control date" id="Tahun">
                        <div id="Tahun-errors"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
