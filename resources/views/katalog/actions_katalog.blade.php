<div class="btn-group" role="group">
    <a href="{{route('katalog.edit', $data->katalog_id)}}" class="btn btn-primary btn-edit btn-sm">
<span class="material-icons" style="font-size: 0.9rem">
edit
</span> Edit
    </a>
    <a href="{{route('katalog.destroy', $data->katalog_id)}}" class="btn btn-danger btn-destroy btn-sm">
    <span class="material-icons" style="font-size: 0.9rem">
delete
</span>
        Hapus
    </a>
</div>
