@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 py-3 mt-3">
                <div>
                    <h2 class="text-capitalize news-title-detail">{{$berita->judul}}</h2>
                </div>

                <div class="d-flex justify-content-center mb-3">
                    <img class="img-fluid img-responsive"
                         width="100%"
                         loading="lazy"
                         src="{{ asset('/uploads/berita/'.$berita->gambar) }}"
                         alt="{{$berita->judul}}">
                </div>

                <div class="d-flex justify-content-between align-items-center mt-3">
                    <div class="text-muted">
                        <span class="text-dark">{{$berita->name}}</span>
                        |
                        <span class="ml-1">{{tanggal_indo($berita->created_at)}}</span>
                        |
                        <span class="fa fa-eye"></span> <small>{{$berita->hit}}</small>
                    </div>
                    <small class="badge badge-pill badge-danger p-2">{{$berita->nama}}</small>
                </div>

                <div class="py-3 news-content overflow-hidden">
                    {!! $berita->isi !!}
                </div>

                <div class="py-3">
                    <x-berita-web type="terbaru" limit="2"></x-berita-web>
                </div>

                <div class="py-3">
                    <x-berita-web type="terpopuler" limit="2"></x-berita-web>
                </div>
            </div>
            <div class="col-lg-4">
                <x-widget-sidebar :long="1"></x-widget-sidebar>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function (){
           $(".news-content").find("img").addClass("img-responsive");
        });
    </script>
@endpush
