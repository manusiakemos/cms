@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 py-3">
                <div>
                    <h2 class="text-uppercase widget-title">produk hukum</h2>
                </div>

                <div class="shadow-sm rounded">
                    <div class="bg-waves-top">
                        <div class="p-3">
                            <div class="form-group">
                                <x-select-component case="reg_tahun" name="tahun" id="tahun" text-value="reg_tahun" text-label="reg_tahun"></x-select-component>
                            </div>
                            <div class="form-group">
                                <x-select-component case="pengusul_id" name="pengusul" id="pengusul" text-value="id" text-label="nama"></x-select-component>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-3 mt-3">
                        <table id="datatables" class="table table-striped dt-responsive"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/select2/select2.min.css') }}"/>
@endpush


@push("script")
    <script src="{{ asset('vendor/crudgen/libs/mask/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/select2/select2.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset("vendor/crudgen/libs/datatables/datatables.min.js") }}"></script>
    <script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
    <script>
        var configDt = {
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json"
            },
            "dom": 'lfrtip',
            "ajax": {
                "url": '{{url()->current()}}',
                "data": {
                    "tahun": null,
                    "pengusul": null
                }
            },
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "order": [
                [1, "desc"]
            ],
            "columns": [
                {
                    "data": "nama",
                    "name": "propem_pengusul.nama",
                    "orderable": true,
                    "searchable": true,
                    "title": "Pengusul",
                    "class": "auto text-capitalize"
                },
                {
                    "data": "judul",
                    "name": "judul",
                    "orderable": true,
                    "searchable": true,
                    "title": "Judul",
                    "class": "auto text-capitalize"
                },
                {
                    "data": "no",
                    "name": "no",
                    "orderable": true,
                    "searchable": true,
                    "title": "No",
                    "class": "auto text-capitalize"
                },
                {
                    "data": "tanggal",
                    "name": "tanggal",
                    "orderable": true,
                    "searchable": true,
                    "title": "Tanggal Diundangkan",
                    "class": "auto text-capitalize"
                },
                {
                    "data": "tahun",
                    "name": "tahun",
                    "orderable": true,
                    "searchable": true,
                    "title": "Tahun",
                    "class": "auto text-capitalize"
                }
            ]

        };

        $(document).ready(function () {
            window.dt = $('#datatables').DataTable(configDt)
        });

        $("#tahun").on("change", function (e) {
            configDt.ajax.data.tahun = $(this).val();
            dt.destroy();
            dt = $('#datatables').DataTable(configDt)
        });
        $("#pengusul").on("change", function (e) {
            configDt.ajax.data.pengusul = $(this).val();
            dt.destroy();
            dt = $('#datatables').DataTable(configDt)
        });
    </script>
@endpush
