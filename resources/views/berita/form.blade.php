<div>
    <form action="{{$action}}" id="formModal" method="POST">
        @csrf
        @isset($method)
            @if($method == "PUT")
                @method("PUT")
            @endif
        @endisset
        <div class="content">
            <div class="class1">
                <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="mt-4">
                <div class="form-group">
                    <label for="kategori_id">Kategori</label>
                    <x-select-component id="kategori_id"
                                        name="kategori_id"
                                        case="kategori_berita"
                                        text-value="kategori_id"
                                        text-label="nama"
                                        error-container="#kategori_id-errors"
                                        :selected="isset($data) ? $data->kategori_id : ''">
                    </x-select-component>
                    <div id="kategori_id-errors"></div>
                </div>
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input name="judul" type="text"
                           data-parsley-errors-container="#judul-errors"
                           value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                           class="form-control" id="judul">
                    <div id="judul-errors"></div>
                </div>
               {{-- <div class="form-group">
                    <label for="url">URL</label>
                    <input name="url" type="text"
                           data-parsley-errors-container="#url-errors"
                           value="<?php echo e(isset($data) ? $data->url : ""); ?>"
                           class="form-control" id="url">
                    <div id="url-errors"></div>
                </div>--}}
                <div class="form-group">
                    <div class="custom-file mt-4">
                        <input name="gambar" type="file"
                               class="custom-file-input" id="gambar">
                        <label class="custom-file-label" for="gambar">Gambar</label>
                    </div>
                </div>

<!--                <div class="form-group">
                    <a id="lfm" data-input="thumbnail" data-type="Files" data-preview="holder" class="btn btn-primary text-white">
                        <i class="fa fa-file-pdf-o"></i> Buka File Manager
                    </a>
                </div>-->

                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea name="isi" data-parsley-errors-container="#isi-errors"
                              id="isi">{!! isset($data) ? $data->isi : "" !!}</textarea>
                    <div id="isi-errors"></div>
                </div>


            </div>
            <div class="class4">
                <button type="submit" class="btn btn-primary" id="save">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </form>
</div>

@push("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/select2/select2.min.css') }}"/>
@endpush


@push("script")
    <script src="{{ asset('vendor/crudgen/libs/mask/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/vfs_fonts.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset("vendor/crudgen/libs/datatables/datatables.min.js") }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/loadingoverlay.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/jqueryform.js') }}"></script>

    <script src="{{asset('vendor/crudgen/libs/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/crudgen/libs/ckeditor/adapters/jquery.js')}}"></script>
    <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
    <script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('Files', {prefix: "{{url('filemanager')}}"});

            $('#isi').ckeditor(function () { /* callback code */
            }, {
                language: 'id',
                height: '800px',
                filebrowserBrowseUrl: '{{url('filemanager')}}',
                iframebrowserBrowseUrl: '{{url('filemanager')}}',
            });

            /*CKEDITOR.editorConfig = function( config ) {
                config.extraPlugins = 'mediabrowser';
            };*/


            $("#formModal").on("submit", function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var serializedArray = $(this).serializeArray();
                $.each(serializedArray, function (index, value) {
                    let el = $(`#${value.name}-errors`);
                    el.html("");
                });
                $(this).ajaxSubmit({
                    success: function (response) {
                        alertify.success(response.message);
                        dt.ajax.reload(null, false);
                    },
                    error: function (error) {
                        alertify.error(error.responseJSON.message);
                        var errorBags = error.responseJSON.errors;
                        console.log(errorBags);
                        $.each(errorBags, function (index, value) {
                            let el = $(`#${index}-errors`);
                            el.html(`<small class='text-danger'>${value.join()}</small>`);
                        });
                    }
                });
                return false;
            });
        })
        ;
    </script>
@endpush

