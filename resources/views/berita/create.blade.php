@extends('vendor.admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="shadow-sm breadcrumbnav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('berita.index')}}">Berita</a></li>
                        <li class="breadcrumb-item active">{{$modal_title}}</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <div class="card border-light shadow-sm">
                    <div class="card-body">
                        <div id="app">
                            @include("berita.form", [ 'action' => route('berita.store') ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
