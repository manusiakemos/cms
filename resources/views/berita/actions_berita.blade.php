<div class="btn-group" role="group">
    <a href="{{route('berita.edit', $data->id)}}" class="btn btn-primary btn-sm">
<span class="material-icons" style="font-size: 0.9rem">
edit
</span> Edit
    </a>
    <a href="{{route('berita.destroy', $data->id)}}" class="btn btn-danger btn-destroy btn-sm">
    <span class="material-icons" style="font-size: 0.9rem">
delete
</span>
        Hapus
    </a>

    <a href="{{route('view.berita', $data->url)}}" class="btn btn-danger btn-dark btn-sm">
    <span class="material-icons" style="font-size: 0.9rem">
        web
</span>
        Lihat Berita
    </a>
</div>

