@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 py-3">
                <div>
                    <h2 class="text-uppercase widget-title">galeri</h2>
                </div>
                <div id="galeri" class="row">
                    @foreach($galeri as $index => $value)
                        <div class="col-lg-3 box show" style="transition-delay: 0s;">
                            <a href="{{asset('uploads/galeri/'.$value->filename)}}" class="galeri"
                               data-glightbox="title: {{$value->judul}}; description: .custom-desc{{$index}}">
                                <img src="{{asset('uploads/galeri/thumbs_'.$value->filename)}}" class="img-responsive" alt="thumbnail"/>
                            </a>
                            <div class="glightbox-desc custom-desc{{$index}}">
                                <p>
                                    {{$value->keterangan }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-lg-4">
                <div class="py-3 p-3">
                    <x-widget-sidebar></x-widget-sidebar>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("script")
    <script src="{{asset('vendor/crudgen/libs/glightbox/js/glightbox.min.js')}}"></script>
    <script>
        const galeri = GLightbox({
            selector: '.galeri',
            touchNavigation: true,
            loop: true,
            autoplayVideos: true
        });
    </script>
@endpush

@push("style")
    <link href="{{asset('vendor/crudgen/libs/glightbox/css/glightbox.min.css')}}" rel="stylesheet"
          type="text/css">
@endpush
