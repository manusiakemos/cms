@isset($configApp['core'])
    @if(isset($configApp['core']['background_header']))
        <div class="web-header-banner py-3"
             style="background:url(' {{asset('uploads/'.$configApp['core']['background_header'])}} ');background-position: center;background-size: cover;background-repeat: no-repeat;">
            @else
                <div class="web-header-banner py-3 header-bg">
                    @endif
                    <div class="container">
                        <div class="d-flex align-items-center justify-content-between branding-wrap">
                            <div class="ml-3 branding d-flex align-items-center">
                                @isset($configApp['core']['logo'])
                                    <img src="{{asset('uploads/'.$configApp['core']['logo'])}}" alt="logo"
                                         height="42px">
                                @endisset
                                <div class="ml-3 d-flex flex-column justify-content-start align-items-start ">
                                    <h1 class="large-text  d-none d-xl-block"
                                        id="nama-aplikasi">{{$configApp['core']['nama_aplikasi']}}</h1>
                                    <h6 class="large-text d-xl-none"
                                        id="nama-aplikasi">{{$configApp['core']['nama_aplikasi']}}</h6>
                                    <h6 class="d-none d-xl-block small-text font-weight-bold"
                                        id="nama-aplikasi-alt">{{$configApp['core']['nama_aplikasi_alt']}}</h6>
                                </div>
                            </div>
                            @isset($configApp['core']['logo_alt'])
                                <img src="{{asset('uploads/'.$configApp['core']['logo_alt'])}}" alt="logo_alt"
                                     height="42px">
                            @endisset
                        </div>
                    </div>
                </div>
    @endisset
