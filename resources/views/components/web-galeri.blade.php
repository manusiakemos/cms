<div class="row">
    <div class="col-12">
        <h4 class="widget-title text-center">GALERI</h4>
    </div>
    <div class="col-12">
        <div class="row justify-content-center align-items-center" style="flex:1" id="galley">
            @foreach($gallery as $index => $value)
                <div class="col-lg-3 mb-3 d-flex justify-content-center" style="transition-delay: 0s;">
                    <a href="{{asset('uploads/galeri/'.$value->filename)}}" class="galeri"
                       data-glightbox="title: {{$value->judul}}; description: .custom-desc{{$index}}">
                        <img src="{{asset('uploads/galeri/thumbs_'.$value->filename)}}" loading="lazy" alt="thumbnail"/>
                    </a>
                    <div class="glightbox-desc custom-desc{{$index}}">
                        <p>
                            {{$value->keterangan }}
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@push("script")
    <script src="{{asset('vendor/crudgen/libs/glightbox/js/glightbox.min.js')}}"></script>
    <script>
        const galeri = GLightbox({
            selector: '.galeri',
            touchNavigation: true,
            loop: true,
            autoplayVideos: true
        });
    </script>
@endpush

@push("style")
    <link href="{{asset('vendor/crudgen/libs/glightbox/css/glightbox.min.css')}}" rel="stylesheet"
          type="text/css">
@endpush
