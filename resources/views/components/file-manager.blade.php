<button class="btn btn-primary btn-block mt-3 mb-3 pb-3 " type="button" data-toggle="collapse" data-target="#file-manager-embed" aria-expanded="false" aria-controls="multiCollapseExample2">
    File Manager
</button>
<div id="file-manager-embed">
    <iframe src="{{url('/admin/filemanager')}}" style="width: 100%; height: 100vh; overflow: hidden; border: none;"></iframe>
</div>
