@isset($configApp['core'])
    <footer class="bg-footer">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-4">
                    @isset($configApp['core']['alamat'])
                        <h4 class="widget-title text-uppercase">ALAMAT</h4>
                        <span class="font-weight-bold">
                            {!! $configApp['core']['alamat'] !!}
                        </span>
                    @endisset
                    <div class="mt-3">
                        <h4 class="widget-title text-uppercase">KONTAK</h4>
                        <div>
                            @foreach($kontak as $value)
                                <div class="d-flex align-items-center mb-3">
                                    <div>
                                        <img src="{{asset('uploads/'.$value->icon)}}" alt="" style="height: 40px;">
                                    </div>
                                    <div class="pl-3 font-weight-bolder">
                                        {!! $value->html !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <x-web-link-terkait></x-web-link-terkait>
                </div>
                <div class="col-lg-5">
                    @isset($configApp['core']['google_maps'])
                       <div class="overflow-hidden">
                           {!! $configApp['core']['google_maps'] !!}
                       </div>
                    @endisset
                </div>
            </div>
        </div>
        <div class="footer p-5" style="width: 100%; background: #111">
            <div class="d-flex justify-content-center">
                <span class="text-footer text-uppercase font-weight-bolder">
                    @isset($configApp['core']['nama_aplikasi'])
                        HAK CIPTA <span
                            class="fa fa-copyright"></span> {{date('Y')}} {{$configApp['core']['nama_aplikasi']}}
                    @endisset
                </span>
            </div>
        </div>
    </footer>

@endisset
