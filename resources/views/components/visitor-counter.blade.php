<div>
    <h6 class="widget-title text-uppercase">Jumlah Kunjungan</h6>
    <div class="card border-none bg-visitor">
        <div class="card-body">
            <div class="mt-3">
                <div class="d-flex justify-content-between align-items-center">
                   <h6 class="ml-3 text-uppercase text-white-50 font-weight-bold">Kunjungan Hari Ini</h6>  <h4 class="text-white font-weight-bold">{{$visitor->hari_ini}}</h4>
                </div>
            </div>
            <div class="mb-1">
                <div class="d-flex justify-content-between align-items-center">
                   <h6 class="ml-3 text-uppercase text-white-50 font-weight-bold">Kunjungan Bulan Ini</h6>   <h4 class="text-white font-weight-bold">{{$visitor->bulan_ini}}</h4>
                </div>
            </div>
            <div class="mb-1">
                <div class="d-flex justify-content-between align-items-center">
                    <h6 class="ml-3 text-uppercase text-white-50 font-weight-bold">Semua Kunjungan</h6>  <h4 class="text-white font-weight-bold">{{$visitor->total}}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
