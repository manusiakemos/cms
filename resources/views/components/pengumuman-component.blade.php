@if(count($data) > 0)
    <div class="ticker-container d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ticker d-none">
                         <span class="breaking-news font-weight-bold mr-3 text-primary">Pengumuman:</span>
                        <ul>
                            @foreach($data as $value)
                                <li><span class="font-weight-bold">{!! $value->judul !!}</span> |
                                    <span>{!! $value->isi !!}</span
                                        ></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@push('script')
    @if(count($data) > 0)
        <script src="{{ asset('vendor/newsticker/jquery.ticker.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('.ticker').ticker({
                    random: false, // Whether to display ticker items in a random order
                    itemSpeed: 3000,  // The pause on each ticker item before being replaced
                    cursorSpeed: 15,    // Speed at which the characters are typed
                    pauseOnHover: false,  // Whether to pause when the mouse hovers over the ticker
                    finishOnHover: false,  // Whether or not to complete the ticker item instantly when moused over
                    cursorOne: '_',   // The symbol for the first part of the cursor
                    cursorTwo: '-',   // The symbol for the second part of the cursor
                    fade: true,  // Whether to fade between ticker items or not
                    fadeInSpeed: 700,   // Speed of the fade-in animation
                    fadeOutSpeed: 500    // Speed of the fade-out animation
                });
                $(".ticker").removeClass("d-none");
            });
        </script>
    @endif
@endpush
