<div class="mt-3">
    <h4 class="text-uppercase widget-title text-center">BERITA {{$type}}</h4>
    <div class="row">
        @if(count($data) > 0)
            @foreach($data as $value)
              <div class="col-lg-6 card-group">
                  @include('components.berita-component', compact('value'))
              </div>
            @endforeach
        @else
            <div class="col-12">
                <div class="py-3">
                    <h4 class="text-center">Belum Ada Berita</h4>
                </div>
            </div>
        @endif
    </div>
</div>

