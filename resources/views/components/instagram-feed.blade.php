<div class="row">
    <div class="col-12">
        <h4 class="widget-title text-center mb-3 text-uppercase">Instagram</h4>
        <div class="card-columns justify-content-center">
            @foreach($feed as $index => $value)
                @if($index < $limit)
                    <div class="card">
                        <img class="card-img-top" src="{{$value['url']}}" alt="Card image cap">
                        @if($value['caption'])
                            <div class="card-body">
                                <p class="card-text">{{$value['caption']}}</p>
                            </div>
                        @endif
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>


