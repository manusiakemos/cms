<div class="card mb-3 shadow-sm border-0">
    <img
        src="{{ $value->gambar ? asset('uploads/berita/thumbs_'.$value->gambar) : asset('images/city.png') }}"
        alt="{{$value->judul}}" class="card-img-top" loading="lazy">
    <div class="card-body">
        <div class="d-lg-flex justify-content-between">
            <div class="mr-3">
                <span class="badge badge-danger badge-pill p-2"> {{$value->nama}} </span>
            </div>
            <div>
                <small>{{tanggal_indo($value->created_at)}}</small>
            </div>
        </div>
        <div class="mt-3 mb-3">
            <span class="text-capitalize news-title text-dark">{{$value->judul}}</span>
        </div>

        <div class="mt-3 mb-3 overflow-hidden">
            {!! \Illuminate\Support\Str::limit($value->isi,100) !!}
        </div>

        <div class="d-flex justify-content-between align-items-center">
            <div class="d-inline-block">
                <span class="fa fa-eye font-weight-bold"></span> <span class="ml-1 font-weight-bold">{{$value->hit}}</span>
            </div>
            <div>
                <a class="btn btn-outline-danger btn-block text-uppercase font-weight-bold" href="{{route('view.berita', $value->url)}}">selengkapnya</a>
            </div>
        </div>
    </div>
</div>
