<div class="list-group list-group-flush">
    @foreach($data as $value)
        <div class="list-group-item">
            <a href="{{route('view.regulasi', [\Illuminate\Support\Str::slug($value->reg_judul)])}}" class="d-lg-flex justify-content-between">
                <h6 class="text-uppercase">{{$value->reg_judul}}</h6>
                <div class="ml-lg-3">
                    <span class="badge badge-primary badge-pill p-2"> {{$value->kategori_nama}} </span>
                </div>
            </a>
        </div>
    @endforeach
</div>
