
<div class="border-0 mb-3 shadow-sm" id="votingApp">
    <h4 class="widget-title">POLLING</h4>
    <div class="card no-border " v-if="!isVoted">
        <div class="card-body">
            <p class="text-uppercase">
                Bagaimana layanan {{ config('configApp')['core']['nama_aplikasi'] }}
            </p>


            <form action="{{route('voting')}}" method="POST" id="formVoting">
                @csrf

                @foreach($data as $key => $value)
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio{{$key}}"
                               name="voted" class="custom-control-input" value="{{$key}}">
                        <label class="custom-control-label text-capitalize"
                               for="customRadio{{$key}}">{{$value->label}}</label>
                    </div>
                @endforeach

                <div class=form-group">
                   {!! captcha_img('flat') !!}
                </div>

                <div class="form-group">
                    <input type="text" name="captcha" class="form-control" placeholder="Masukkan text pada gambar">
                </div>

                <div class="mt-3">
                    <button class="btn btn-block btn-primary" type="submit">Kirim Polling</button>
                </div>
            </form>
        </div>
    </div>

    <div v-else>
        <div class="card">
            <div class="card-body">
                <p class="font-weight-bold">@{{ result.message }}</p>
                <div v-for="(v,i) in result.data.data" :key="i" >
                    <span class="text-capitalize font-weight-bold">@{{ v.label }} (@{{ v.value }})</span>
                    <div class="progress mb-3" style="height: 30px; background-color: #1a202c" >
                        <div class="progress-bar" role="progressbar"
                             :style="`width: ${v.percent}%;`"
                             :aria-valuenow="v.percent"
                             aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="font-weight-bold text-white" style="font-size: 14px;">@{{v.percent}}%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mb-3"></div>

@push('scriptAfter')
    <script src="{{asset('vendor/crudgen/libs/etc/jqueryform.js')}}"></script>
    <script>
        $(document).ready(function (){
            var isVoted =  localStorage.getItem("isVoted");
            var votingApp = new Vue({
                el:"#votingApp",
                data:{
                    isVoted: isVoted,
                    result:{}
                },
                mounted(){
                    if (isVoted){
                        console.log("isVoted :" + this.isVoted);
                        this.getResult();
                    }
                },
                methods:{
                    getResult(){
                        var me = this;
                        axios.get('/voting').then(res=>{
                           me.result = res.data;
                        });
                    }
                }
            });

            $("#formVoting").on("submit", function(e){
                e.preventDefault();
                $(this).ajaxSubmit({
                    success: function (res) {
                        localStorage.setItem("isVoted", true);
                        votingApp.isVoted = true;
                        votingApp.result = res;
                        alert('voting berhasil dikirim');
                    },
                    error: function (res){
                        var response = res.responseJson;
                        alert(response.message);
                    }
                });
            });
        });
    </script>
@endpush
