<div class="overflow-hidden">
    <!-- Slider main container -->
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            @foreach($sliders as $slide)
                <figure class="swiper-slide"
                     style="border-radius: 0;
                         background-repeat: no-repeat;
                         background-position: center center;
                         background-size: cover;
                         background-image: url('{{ asset('uploads/slide/'.$slide->gambar) }}')">
                    <div class="slider-text">
                        <div style="position: absolute; bottom: 40px; left: 0; right: 0">
                            <div class="text-center d-block">{{$slide->nama}}</div>
                            <div class="text-center d-block">
                                <small>{{$slide->isi}}</small>
                            </div>
                        </div>
                    </div>
                </figure>
            @endforeach
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>


@push('script')
    <script>
        var swiper = new Swiper('.swiper-container', {
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: {
                delay: 3500,
                disableOnInteraction: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination'
            },
            mousewheel: false,
            keyboard: true,
        });
    </script>
@endpush
