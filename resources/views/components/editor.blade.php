<div>
    <textarea name="{{$name}}" id="{{$id}}"
              data-parsley-errors-container="{{$errorContainer}}">{!! isset($value) ? $value : "" !!}</textarea>
</div>

@push("script")
    <script src="{{asset('vendor/crudgen/libs/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/crudgen/libs/ckeditor/adapters/jquery.js')}}"></script>
    <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#{{$id}}').filemanager('Files', {prefix: "{{url('filemanager')}}"});

            $('#{{$id}}').ckeditor(function () { /* callback code */
            }, {
                language: 'id',
                height: '800px',
                filebrowserBrowseUrl: '{{url('filemanager')}}',
            });
        });
    </script>
@endpush
