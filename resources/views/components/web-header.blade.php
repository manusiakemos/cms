@include('components.header-title')
<nav class="bg-navbar">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Mobile menu toggle button (hamburger/x icon) -->
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <ul id="main-menu" class="sm sm-blue">
                    @foreach($menus as $menu)
                        @if($menu->type == 'link')
                            <li><a href="{{url($menu->data ?? "/")}}">{{$menu->text}}</a></li>
                        @elseif($menu->type == 'halaman')
                            <li><a class="nav-link" href="{{url('/halaman/'.$menu->data ?? "/")}}">{{$menu->text}}</a></li>
                        @elseif($menu->type == 'dropdown')
                            @include('includes.menu_item', ['menu' => $menu])
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>


@push('script')
    <link rel="stylesheet" href="{{ asset('vendor/smartmenus/css/sm-core-css.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/smartmenus/css/navbar.css') }}">
    <script src="{{ asset('vendor/smartmenus/jquery.smartmenus.js') }}"></script>
    <script>
        $(function() {
            $('#main-menu').smartmenus({
                mainMenuSubOffsetX: -1,
                mainMenuSubOffsetY: 4,
                subMenusSubOffsetX: 6,
                subMenusSubOffsetY: -6
            });
        });

        // SmartMenus mobile menu toggle button
        $(function() {
            var $mainMenuState = $('#main-menu-state');
            if ($mainMenuState.length) {
                // animate mobile menu
                $mainMenuState.change(function(e) {
                    var $menu = $('#main-menu');
                    if (this.checked) {
                        $menu.hide().slideDown(250, function() { $menu.css('display', ''); });
                    } else {
                        $menu.show().slideUp(250, function() { $menu.css('display', ''); });
                    }
                });
                // hide mobile menu beforeunload
                $(window).bind('beforeunload unload', function() {
                    if ($mainMenuState[0].checked) {
                        $mainMenuState[0].click();
                    }
                });
            }
        });
    </script>
@endpush
