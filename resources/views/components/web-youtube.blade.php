<div>
    @if(count($data) > 0)
        <div>
            <h6 class="widget-title text-uppercase">Video</h6>
        </div>
    @endif

   <div>
       @foreach($data as $value)
           <div class="overflow-hidden mb-3 mt-3">
               @if(isset($value->name))
                   <h4>{{$value->name}}</h4>
                   @endif
               <div>
                   {!! $value->embed !!}
               </div>
                   @if(isset($value->caption))
                        <small>{{$value->caption}}</small>
                   @endif
           </div>
       @endforeach
   </div>
</div>
