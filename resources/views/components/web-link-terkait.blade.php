<div>
    <h4 class="widget-title">LINK TERKAIT</h4>
    <div>
        @foreach($data as $value)
            <a href="{{$value->url}}">
                <div class="d-flex align-items-center justify-content-start mb-3">
                    <div class="d-flex justify-content-center align-items-center">
                        <img src="{{ asset('/uploads/linkterkait/'.$value->gambar) }}" alt="link-terkait" width="70">
                    </div>
                    <div class="ml-3">
                        <span class="font-weight-bold text-footer">{{$value->name}}</span>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
</div>

