<div>
    @if($data)
        <div class="mb-3 overflow-hidden">
            @php($img= asset('uploads/'.$ketua_image))
            <div
                style="background: url('{{$img}}');
                    height: 300px;
                    background-size: cover;
                    background-position: center center;
                    border-radius: 10px;
                    border: none;
                    position: relative">
                <div style="position: absolute;
                    bottom: 0px;
                    z-index: 1;
                    height: auto;
                    width: 100%;
                    background: rgba( 30, 30, 30, 0.15 );backdrop-filter: blur( 4px );border-radius: 10px">
                    <div class="p-3">
                        <div class="mb-1">
                            <h4 class="text-white">{{$ketua_nama}}</h4>
                        </div>
                        <span class="text-white">{{$ketua_jabatan}}</span>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
