<div  class="py-3 mt-3">
    <div class="mb-3">
        <form action="{{route('berita')}}" method="GET">
            @php($s = request()->get('s') ? request()->get('s') : '')
            <div class="input-group">
                <input name="s" type="text" class="form-control input-search" placeholder="Pencarian" value="{{$s}}">
                <button class="btn btn-outline-primary" type="submit"><span class="fa fa-search"></span></button>
            </div>
        </form>
    </div>

    <div class="mb-3 overflow-hidden">
        <x-visitor-counter></x-visitor-counter>
    </div>

    <x-kepala-skpd></x-kepala-skpd>


    @if($long)
        <div id="gpr-kominfo-widget-container" class="mb-3"></div>


        <div class="mb-3 overflow-hidden">
            <x-voting></x-voting>
        </div>

        <div class="mb-3">
            <x-web-youtube></x-web-youtube>
        </div>
    @endif
</div>

@if($long)
    @push('script')
        <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
    @endpush
@endif
