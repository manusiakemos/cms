@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="py-3 mt-3">
                    <x-pengumuman-component></x-pengumuman-component>
                </div>
                <section>
                    <x-slider-web></x-slider-web>
                </section>

                <section>
                    <div>
                        <x-berita-web type="terbaru" limit="4"></x-berita-web>
                    </div>
                    <div class="mt-3">
                        <x-berita-web type="terpopuler" limit="4"></x-berita-web>
                    </div>
                </section>
            </div>
            <div class="col-lg-4">
                <x-widget-sidebar :long="true"></x-widget-sidebar>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="separator"></div>
    </div>

    @if(config('instagram-feed.enabled'))
        <section class="bg-dark py-3 overflow-hidden">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-12 ">
                        <x-instagram-feed :limit="9"></x-instagram-feed>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="bg-white py-3">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 ">
                    <x-web-galeri :limit="8"></x-web-galeri>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styleBefore')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
@endpush

@push('scriptBefore')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
@endpush
