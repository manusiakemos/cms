@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 py-3">
                <div>
                    <h2 class="text-uppercase widget-title">{{$data->judul}}</h2>
                </div>

                <div class="py-1">
                    <small>{{tanggal_indo($data->tanggal, false, false)}}</small>
                </div>

                <div class="py-3">
                    {!! $data->isi !!}
                </div>
            </div>
        </div>
    </div>
@endsection
