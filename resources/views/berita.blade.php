@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 py-3">
                <div class="row">
                    <div class="col-lg-8">
                        @php($s = request()->get('s'))
                        @if($s)
                            <div class="row">
                                <div class="col-12">
                                    <div class="mt-3 mb-3">
                                        menampilkan berita dengan keyword "{{request()->get('s')}}"
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            @foreach($berita as $value)
                                <div class="col-lg-6 card-group">
                                    @include('components.berita-component', compact('value'))
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                {{$berita->links()}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <x-widget-sidebar :long="true"></x-widget-sidebar>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
