@extends('vendor.admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="shadow-sm breadcrumbnav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Menu</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <div class="card border-light shadow-sm">
                    <div class="card-body">
                        <h4 class="card-title mb-3">Menu Editor</h4>
                        <div class="mt-3">
                            @verbatim
                                <div id="menuApp" class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-primary btn-create mb-3" id="create" @click="create()">
                                            <span class="fa fa-plus"></span>
                                            Tambah
                                        </button>
                                        <button class="btn btn-primary btn-create mb-3" id="refresh" @click="refresh()">
                                            <span class="fa fa-recycle"></span>
                                            Refresh
                                        </button>
                                        <button class="btn btn-primary btn-create mb-3" id="refresh" @click="save()">
                                            <span class="fa fa-save"></span>
                                            Simpan
                                        </button>
                                    </div>
                                    <div class="col-lg-12">
                                        <nested-draggable v-model="list"/>
                                    </div>

                                    <div id="modalAjaxContainer">
                                        <form method="POST" @submit.prevent="save()">
                                            <div class="modal fade" id="modalForm">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">

                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">{{selected.text}}</h4>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>

                                                        <!-- Modal body -->
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control"
                                                                       placeholder="Text"
                                                                       v-model="selected.text">
                                                            </div>
                                                            <div class="form-group">
                                                                <select v-model="selected.type" class="form-control">
                                                                    <option disabled value="">Pilih Tipe</option>
                                                                    <option v-for="v in tipe" :value="v.value">
                                                                        {{v.text}}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group" v-if="selected.type == 'link'">
                                                                <input type="text" class="form-control"
                                                                       v-model="selected.data">
                                                            </div>
                                                            <div class="form-group"
                                                                 v-else-if="selected.type == 'halaman'">
                                                                <select v-model="selected.data" class="form-control">
                                                                    <option disabled value="">Pilih Halaman</option>
                                                                    <option v-for="v in halaman" :value="v.url">
                                                                        {{v.judul}}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Modal footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary btn-submit"
                                                                    @click="save()">
                                                                Simpan
                                                            </button>
                                                            <button type="button" class="btn btn-danger"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            @endverbatim
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/select2/select2.min.css') }}"/>
    <style>
        .item-container-fluid {
            max-width: 80%;
            margin: 0;
        }

        .item {
            padding: 1.2rem;
            background-color: #fefefe;
            margin: 5px;
        }

        .item-sub {
            margin-left: 1.2rem;
        }

        .dragArea {
            min-height: 80px;
            outline: 1px dashed;
        }
    </style>
@endpush


@push("script")
    <script src="{{ asset('vendor/crudgen/libs/mask/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/vfs_fonts.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset("vendor/crudgen/libs/datatables/datatables.min.js") }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/loadingoverlay.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/jqueryform.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>

    @verbatim
        <script>
            $("body").on("click", "#create", function () {
                console.log('clicked');
                $("#modalForm").modal().show();
            });

            Vue.component('nested-draggable', {
                props: {
                    value: {
                        required: false,
                        type: Array,
                        default: null
                    },
                    list: {
                        required: false,
                        type: Array,
                        default: null
                    }
                },
                name: "nested-draggable",
                template: `
                <draggable
                v-bind="dragOptions"
                tag="div"
                class="dragArea"
                :list="list"
                :value="value"
                @input="emitter">
                <div class="item-group" :key="el.id" v-for="el in realValue">
                  <div class="item d-flex">
                      <div>{{ el.text }}</div>
                      <div class="ml-auto">
                        <button class="btn btn-primary" @click="edit(el)">Edit</button>
                        <button class="btn btn-danger" @click="destroy(el)">Hapus</button>
                      </div>
                    </div>
                    <nested-draggable v-if="el.type=='dropdown'" class="item-sub" :list="el.sub" />
                  </div>
                </draggable>`,
                methods: {
                    emitter(value) {
                        this.$emit("input", value);
                    },
                    edit(el) {
                        this.$root.edit(el);
                    },
                    destroy(el) {
                        this.$root.destroy(el);
                    },
                },
                computed: {
                    dragOptions() {
                        return {
                            animation: 0,
                            group: "description",
                            disabled: false,
                            ghostClass: "ghost"
                        };
                    },
                    // this.value when input = v-model
                    // this.list  when input != v-model
                    realValue() {
                        return this.value ? this.value : this.list;
                    }
                },
            });

            var app = new Vue({
                el: '#menuApp',
                data: {
                    list: [],
                    halaman: [],
                    isEdit: true,
                    selected: {
                        "id": "",
                        "text": "",
                        "type": "",
                        "data": "",
                        "sub": [],
                    },
                    clonedSelected: {},
                    tipe: [
                        {
                            value: 'link',
                            text: 'Link',
                        },
                        {
                            value: 'dropdown',
                            text: 'Dropdown',
                        },
                        {
                            value: 'halaman',
                            text: 'Halaman',
                        }
                    ]
                },
                created() {
                    this.getData();
                    this.getHalaman();
                    this.clonedSelected = this.selected;
                },
                watch: {
                    "selected.type": {
                        handler: function (value, oldvalue) {
                            var vm = this;
                            if (value == 'dropdown') {
                                vm.selected.sub = [];
                            }
                        }
                    }

                },
                methods: {
                    getHalaman() {
                        axios.post('/admin/halaman/api').then(res => {
                            this.halaman = res.data;
                        });
                    },
                    refresh() {
                        alertify.success('Refresh');
                        this.isEdit = true;
                        this.getData();
                    },
                    getData() {
                        this.isEdit = true;
                        axios.post('/admin/menu/api').then(res => {
                            this.list = res.data;
                        });
                    },
                    makeid(length) {
                        var result = '';
                        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                        var charactersLength = characters.length;
                        for (var i = 0; i < length; i++) {
                            result += characters.charAt(Math.floor(Math.random() * charactersLength));
                        }
                        return result;
                    },
                    create() {
                        this.isEdit = false;
                        this.selected = _.cloneDeep(this.clonedSelected);
                        this.selected.id = this.makeid(16);
                        $("#modalForm").modal().show();
                    },
                    edit(el) {
                        this.isEdit = true;
                        this.selected = el;
                        $("#modalForm").modal().show();
                    },
                    destroy(el) {
                        this.$nextTick(() => {
                            this.removeArray(this.list, el.id);
                        });
                        this.isEdit = true;
                        this.save();
                    },
                    removeArray(array, id) {
                        this.isEdit = true;
                        return array.some((o, i, a) => {
                                if (o.id === id) {
                                    return a.splice(i, 1);
                                } else {
                                    return this.removeArray(o.sub || "", id);
                                }
                            }
                        );
                    },
                    save() {
                        if (this.isEdit == false) {
                            this.list.push(this.selected);
                        }
                        this.$nextTick(() => {
                            axios.post('/admin/menu/save', {data: this.list}).then(res => {
                                alertify.success('Menu berhasil disimpan')
                            });
                        });
                        setTimeout(() => {
                            this.getData();
                        }, 2000)
                    }
                }
            })
        </script>
    @endverbatim
@endpush
