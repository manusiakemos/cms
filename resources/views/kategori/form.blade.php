<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" >
            @csrf
            @isset($method)
            @if($method == "PUT")
            @method("PUT")
            @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
            <label for="kategori_nama">Nama</label>
            <input name="kategori_nama" type="text"
                   data-parsley-errors-container="#kategori_nama-errors"
                   value="<?php echo e(isset($data) ? $data->kategori_nama : ""); ?>"
                   class="form-control" id="kategori_nama">
            <div id="kategori_nama-errors"></div>
        </div>
                    <div class="form-group">
            <label for="kategori_aktif">Aktif</label>
            <x-radio-component id="kategori_aktif"
               name="kategori_aktif"
               error-container="#kategori_aktif-errors"
               case="status"
               :selected="isset($data) ? $data->kategori_aktif : 1">
            </x-radio-component>
            <div id="kategori_aktif-errors"></div>
        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
