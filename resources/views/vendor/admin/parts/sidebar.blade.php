<aside >
    <nav id="sidebar" class="shadow bg-sidebar">
        <div style="max-height: 90vh; overflow-y: auto">
            <div class="d-lg-flex align-items-center justify-content-center">
                <h4 class="text-logo text-center p-4">{{config('app.name')}}</h4>
            </div>
            <ul class="list-unstyled components mb-5" id="menu-list">
                <li class="{{ (request()->is('home*')) ? 'active' : '' }}">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li>
                    <a href="#setting" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Setting</a>
                    <ul class="collapse list-unstyled" id="setting">
                        @if(auth()->user()->role == 'super-admin')
                            <li class="{{ request()->route()->getName() == 'settinggroup.index' ? 'active' : '' }}">
                                <a href="{{route('settinggroup.index')}}">Setting Group</a></li>
                        @endif

                            <li class="{{ (request()->is('slider*')) ? 'active' : '' }}">
                                <a href="{{route('slider.index')}}">Slider</a></li>

                            <li class="{{ (request()->is('youtube*')) ? 'active' : '' }}">
                                <a href="{{route('youtube.index')}}">Youtube</a></li>

                            <li class="{{ (request()->is('linkterkait*')) ? 'active' : '' }}">
                                <a href="{{route('linkterkait.index')}}">Link Terkait</a>
                            </li>

                            <li class="{{ (request()->is('kontak*')) ? 'active' : '' }}">
                                <a href="{{route('kontak.index')}}">Kontak</a></li>

                            <li class="{{ request()->route()->getName() == 'setting.index' ? 'active' : '' }}">
                                <a href="{{route('setting.index')}}">Setting</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#berita" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Berita</a>
                    <ul class="collapse list-unstyled" id="berita">
                        <li class="{{ (request()->is('kategoriberita*')) ? 'active' : '' }}">
                            <a href="{{route('kategoriberita.index')}}">Kategori Berita</a>
                        </li>

                        <li class="{{ (request()->is('berita*')) ? 'active' : '' }}">
                            <a href="{{route('berita.index')}}">Berita</a>
                        </li>
                    </ul>
                </li>



                <li class="{{ (request()->is('halaman*')) ? 'active' : '' }}">
                    <a href="{{route('halaman.index')}}">Halaman</a>
                </li>

                <li class="{{ (request()->is('menu*')) ? 'active' : '' }}">
                    <a href="{{route('menu.index')}}">Menu</a>
                </li>

                <li class="{{ (request()->is('gallery*')) ? 'active' : '' }}">
                    <a href="{{route('gallery.index')}}">Gallery</a>
                </li>

                <li class="{{ (request()->is('filemanager*')) ? 'active' : '' }}">
                    <a target="_blank" href="{{ url('/admin/filemanager') }}">Manajemen Berkas</a></li>

                <li class="{{ (request()->is('pengumuman*')) ? 'active' : '' }}">
                    <a href="{{route('pengumuman.index')}}">Pengumuman</a></li>

                {{-- <li class="{{ (request()->is('widget*')) ? 'active' : '' }}">
                     <a href="{{route('widget.index')}}">Widget</a></li>--}}

                <li>
                    <a href="#system" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">System</a>
                    <ul class="collapse list-unstyled" id="system">
                        @if(auth()->user()->role=='super-admin')

                            <li class="{{ (request()->is('usermanagement*')) ? 'active' : '' }}">
                                <a href="{{route('usermanagement.index')}}">User Management</a>
                            </li>

                            <li class="{{ (request()->is('css*')) ? 'active' : '' }}">
                                <a href="{{route('css.show')}}">CSS Editor</a>
                            </li>

                        @endif
                        <li class="{{ (request()->is('myprofile*')) ? 'active' : '' }}">
                            <a href="{{route('profile')}}">Profile</a>
                        </li>
                        <li class="{{ (request()->is('password*')) ? 'active' : '' }}">
                            <a href="{{route('password')}}">Ubah Password</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>


        <div class="sidebar-footer d-flex align-items-center justify-content-center">
            <button class="btn btn-logout btn-primary p-3" id="btn-logout">
                <span class="fa fa-lock"></span> Logout
            </button>
        </div>
    </nav>
</aside>
