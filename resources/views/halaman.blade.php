@extends('layouts.web')

@section('content')
    <div class="container ">
        <div class="row">
            <div class="col-lg-12 py-3">
                <div>
                    <h2 class="text-capitalize news-title-detail text-center">{{$halaman->judul}}</h2>
                </div>

                <div class="py-3 news-content">
                    {!! html_entity_decode($halaman->isi) !!}
                </div>
            </div>
        </div>
    </div>
@endsection

