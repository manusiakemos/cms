@extends('vendor.admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="shadow-sm breadcrumbnav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pengumuman.index')}}"></a></li>
                        <li class="breadcrumb-item active">Pengumuman</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <form action="{{$action}}" id="formModal" method="POST">
                    @csrf
                    @isset($method)
                        @if($method == "PUT")
                            @method("PUT")
                        @endif
                    @endisset
                    <div class="modal-contenst bg-white p-3">
                        <div class="modal-headesr bg-white p-3">
                            <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                        </div>
                        <div class="modal-bodys bg-white p-3">
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input name="judul" type="text"
                                       data-parsley-errors-container="#judul-errors"
                                       value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                                       class="form-control" id="judul">
                                <div id="judul-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input name="tanggal" type="date"
                                       style="max-width: 300px;"
                                       data-parsley-errors-container="#tanggal-errors"
                                       value="<?php echo e(isset($data) ? $data->tanggal : date('Y-m-d')); ?>"
                                       class="form-control" id="tanggal">
                                <div id="tanggal-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="isi">Isi</label>
                                <div class="form-group">
                                    <textarea class="form-control" value="{{isset($data) ? $data->isi : ''}}" id="isi" name="isi"
                                              error-container="#isi-errors"></textarea>
                                    <div id="html-errors"></div>
                                </div>
                                <div id="isi-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="aktif">Aktif</label>
                                <x-radio-component id="aktif"
                                                   name="aktif"
                                                   error-container="#aktif-errors"
                                                   case="status"
                                                   :selected="isset($data) ? $data->aktif : 1">
                                </x-radio-component>
                                <div id="aktif-errors"></div>
                            </div>

                        </div>
                        <div class="modal-footesr bg-white p-3">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/select2/select2.min.css') }}"/>
@endpush


@push("script")
    <script src="{{ asset('vendor/crudgen/libs/etc/loadingoverlay.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/jqueryform.js') }}"></script>
    <script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
    <script>
        $("#formModal").on("submit", function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var serializedArray = $(this).serializeArray();
            $.each(serializedArray, function (index, value) {
                let el = $(`#${value.name}-errors`);
                el.html("");
            });
            $(this).ajaxSubmit({
                success: function (response) {
                    alertify.success(response.message);
                    dt.ajax.reload(null, false);
                },
                error: function (error) {
                    alertify.error(error.responseJSON.message);
                    var errorBags = error.responseJSON.errors;
                    console.log(errorBags);
                    $.each(errorBags, function (index, value) {
                        let el = $(`#${index}-errors`);
                        el.html(`<small class='text-danger'>${value.join()}</small>`);
                    });
                }
            });
            return false;
        });
    </script>
@endpush
