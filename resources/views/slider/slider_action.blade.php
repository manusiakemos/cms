<a href="{{route('@{classNameLower}.edit', $data->slider_id)}}" class="btn btn-primary btn-edit btn-sm">
    <span class="fa fa-pencil"></span> Edit
</a>
<a href="{{route('@{classNameLower}.destroy', $data->slider_id)}}" class="btn btn-danger btn-destroy btn-sm">
    <span class="fa fa-trash"></span> Hapus
</a>
