<form action="{{$action}}" id="formModal" method="POST">
    @csrf
    @isset($method)
        @if($method == "PUT")
            @method("PUT")
        @endif
    @endisset
    <div>
        <div>
            <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input name="judul" type="text"
                       data-parsley-errors-container="#judul-errors"
                       value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                       class="form-control" id="judul">
                <div id="judul-errors"></div>
            </div>
            <div class="form-group">
                <label for="aktif">Aktif</label>
                <x-radio-component id="aktif"
                                   name="aktif"
                                   error-container="#aktif-errors"
                                   case="status"
                                   :selected="isset($data) ? $data->aktif : 1">
                </x-radio-component>
                <div id="aktif-errors"></div>
            </div>
            <div class="form-group">
                <label for="html">Value</label>
                <x-editor value="{{isset($data) ? $data->isi : ''}}" id="isi" name="isi"
                          error-container="#isi-errors"></x-editor>
                <div id="html-errors"></div>
            </div>
            <div id="isi-errors"></div>

        </div>
        <div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</form>
@push("script")
    <script src="{{ asset('vendor/crudgen/libs/mask/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/crudgen/libs/datatables/vfs_fonts.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset("vendor/crudgen/libs/datatables/datatables.min.js") }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/loadingoverlay.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/etc/jqueryform.js') }}"></script>

    <script src="{{asset('vendor/crudgen/libs/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/crudgen/libs/ckeditor/adapters/jquery.js')}}"></script>
    <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
    <script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#formModal").on("submit", function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var serializedArray = $(this).serializeArray();
                $.each(serializedArray, function (index, value) {
                    let el = $(`#${value.name}-errors`);
                    el.html("");
                });
                $(this).ajaxSubmit({
                    success: function (response) {
                        alertify.success(response.message);
                        dt.ajax.reload(null, false);
                    },
                    error: function (error) {
                        alertify.error(error.responseJSON.message);
                        var errorBags = error.responseJSON.errors;
                        console.log(errorBags);
                        $.each(errorBags, function (index, value) {
                            let el = $(`#${index}-errors`);
                            el.html(`<small class='text-danger'>${value.join()}</small>`);
                        });
                    }
                });
                return false;
            });
        });
    </script>
@endpush
