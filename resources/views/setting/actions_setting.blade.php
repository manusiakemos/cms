<div class="btn-group" role="group">
    <a href="{{route('setting.edit', $data->id)}}" class="btn btn-primary btn-edit btn-sm">
        <span class="material-icons" style="font-size: 0.9rem">edit</span> Edit
    </a>
    @if(auth()->user()->role == 'super-admin')
        <a href="{{route('setting.destroy', $data->id)}}" class="btn btn-danger btn-destroy btn-sm">
            <span class="material-icons" style="font-size: 0.9rem">delete</span>Hapus
        </a>
    @else
        @if(!$data->fixed)
            <a href="{{route('setting.destroy', $data->id)}}" class="btn btn-danger btn-destroy btn-sm">
                <span class="material-icons" style="font-size: 0.9rem">delete</span>Hapus
            </a>
        @endif
    @endif
</div>
