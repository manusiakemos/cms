<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" enctype="multipart/form-data">
            @csrf
            @isset($method)
                @if($method == "PUT")
                    @method("PUT")
                @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="group_id">Group</label>
                        <x-select-component id="group_id"
                                            name="group_id"
                                            case="group_id"
                                            text-value="id"
                                            text-label="group"
                                            error-container="#group_id-errors"
                                            :selected="isset($data) ? $data->group_id : ''">
                        </x-select-component>
                        <div id="group_id-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input name="name" type="text"
                               {{auth()->user()->role == 'admin' ? 'readonly' : ''}}
                               data-parsley-errors-container="#name-errors"
                               value="<?php echo e(isset($data) ? $data->name : ""); ?>"
                               class="form-control" id="name">
                        <div id="name-errors"></div>
                    </div>
                    @if(isset($data->type) && $data->type)
                        @if($data->type == "image")
                            <div class="form-group">
                                <div class="custom-file mt-4">
                                    <input name="image" type="file"
                                           class="custom-file-input" id="image">
                                    <label class="custom-file-label" for="image">Image</label>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <label for="html">Value</label>
                                <textarea class="form-control" id="value" name="value"
                                          error-container="#value-errors">{{isset($data) ? $data->value : ''}}</textarea>
                                <div id="value-errors"></div>
                            </div>
                        @endif
                    @endif


                    <div class="form-group">
                        <label for="active">Aktif</label>
                        <x-radio-component id="active"
                                           name="active"
                                           error-container="#active-errors"
                                           case="boolean"
                                           :selected="isset($data) ? $data->active : 1">
                        </x-radio-component>
                        <div id="active-errors"></div>
                    </div>

                    @if(auth()->user()->role == 'super-admin')
                        <div class="form-group">
                            <label for="fixed">Fixed</label>
                            <x-radio-component id="fixed"
                                               name="fixed"
                                               error-container="#fixed-errors"
                                               case="boolean"
                                               :selected="isset($data) ? $data->fixed : 1">
                            </x-radio-component>
                            <div id="fixed-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <x-select-component
                                case="setting_type"
                                name="type"
                                id="type"
                                :selected="isset($data) ? $data->type : 'text'"
                                error-container="error_status"
                                text-value="value"
                                text-label="text">
                            </x-select-component>
                        </div>
                    @endif

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/crudgen/libs/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>

<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
