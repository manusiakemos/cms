<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @isset(config('configApp')['core'])
        @isset(config('configApp')['core']['nama_aplikasi_alt'])

        <title>{{ isset(config('configApp')['core']['nama_aplikasi']) ? config('configApp')['core']['nama_aplikasi'] : config('configApp')['core']['nama_aplikasi_alt'] }}</title>

        @endisset
    @endisset

    @stack('meta')
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&family=Raleway:wght@400;500;800&display=swap" rel="stylesheet">
    @stack("styleBefore")
    @stack("scriptBefore")

    <link rel="stylesheet" href="{{asset('css/web.css')}}">
    <script src="{{ asset('js/app.js') }}"></script>

    @stack("style")
    @stack("styleAfter")
@isset(config('configApp')['core'])
        @isset(config('configApp')['core']['favicon'])
            <link rel="icon" href="{{asset('uploads/'.config('configApp')['core']['favicon'])}}">
        @endisset
    @endisset
</head>
<body class="d-flex justify-content-center bg-body">
   <div class="wrapper shadow-lg">
       {{--header--}}
       <x-web-header></x-web-header>
       {{--main content--}}
       <main>
           @yield('content')
       </main>

       <x-web-footer></x-web-footer>

       @stack("script")
       @stack("scriptAfter")
   </div>
</body>
</html>
