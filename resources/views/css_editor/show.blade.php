@extends('vendor.admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="shadow-sm breadcrumbnav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Css Editor</li>
                    </ol>
                </nav>
            </div>
            <div class="col-12">
                <button class="btn btn-primary" id="btnSave">Simpan Perubahan</button>

            </div>
            <div class="col-md-12">
                <div id="cssEditor" class="mt-3">
                    {!! $cssContent !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
    <style type="text/css" media="screen">
        #cssEditor {
            position: relative;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            min-height: calc(100vh - 200px);
        }
    </style>
    @endpush


@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/ace.js"></script>

    <script>
        var editor = ace.edit("cssEditor");
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/css");
        var urlAction = "{{url()->current()}}";
        $(document).ready(function (){
           $("#btnSave").on('click', function (){
              axios.post(urlAction,{css:editor.getValue()}).then(res=>{
                  alertify.success(res.data.message);
              });
           });
        });
    </script>
    @endpush
