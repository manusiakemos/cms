<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" enctype="multipart/form-data">
            @csrf
            @isset($method)
                @if($method == "PUT")
                    @method("PUT")
                @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="pengusul_id">Pengusul</label>
                        <x-select-component id="pengusul_id"
                                            name="pengusul_id"
                                            error-container="#pengusul_id-errors"
                                            case="pengusul_id"
                                            textValue="id"
                                            textLabel="nama"
                                            :selected="isset($data) ? $data->pengusul_id : 1">
                        </x-select-component>
                        <div id="pengusul_id-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input name="judul" type="text"
                               data-parsley-errors-container="#judul-errors"
                               value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                               class="form-control" id="judul">
                        <div id="judul-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="no">No</label>
                        <input name="no" type="text"
                               data-parsley-errors-container="#no-errors"
                               value="<?php echo e(isset($data) ? $data->no : ""); ?>"
                               class="form-control" id="no">
                        <div id="no-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Diundangkan</label>
                        <input name="tanggal" type="text"
                               placeholder="YYYY-MM-DD"
                               data-parsley-errors-container="#tanggal-errors"
                               value="<?php echo e(isset($data) ? $data->tanggal : date('Y-m-d')); ?>"
                               class="form-control date" id="tanggal">
                        <div id="tanggal-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input name="tahun" type="text"
                               placeholder="YYYY"
                               data-parsley-errors-container="#tahun-errors"
                               value="<?php echo e(isset($data) ? $data->tahun : date('Y')); ?>"
                               class="form-control date" id="tahun">
                        <div id="tahun-errors"></div>
                    </div>
                   {{--
                   <div class="form-group">
                        <div class="custom-file mt-4">
                            <input name="file" type="file"
                                   class="custom-file-input" id="file">
                            <label class="custom-file-label" for="file">File</label>
                        </div>
                    </div>
                    --}}

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
