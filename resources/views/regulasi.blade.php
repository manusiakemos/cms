@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 py-3">
                <div>
                    <h2 class="text-uppercase widget-title">produk hukum</h2>
                </div>

                <div class="shadow-sm rounded">
                    <div class="bg-waves-top">
                        <div class="p-3">
                            <div class="form-group">
                                <x-select-component case="reg_tahun" name="tahun" id="tahun" text-value="reg_tahun" text-label="reg_tahun"></x-select-component>
                            </div>
                            <div class="form-group">
                                <x-select-component case="kategori" name="kategori" id="kategori" text-value="kategori_id" text-label="kategori_nama"></x-select-component>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-3 mt-3">
                        <table id="datatables" class="table table-striped dt-responsive"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push("style")
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor/crudgen/libs/select2/select2.min.css') }}"/>
@endpush


@push("script")
    <script src="{{ asset('vendor/crudgen/libs/mask/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/crudgen/libs/select2/select2.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset("vendor/crudgen/libs/datatables/datatables.min.js") }}"></script>
    <script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
    <script>
        var configDt = {
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json"
            },
            "dom": 'lfrtip',
            "ajax": {
                "url": '{{url()->current()}}',
                "data": {
                    "tahun": null,
                    "kategori": null
                }
            },
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "order": [
                [2, "desc"]
            ],
            "columns": [
                {
                    "data": "kategori_nama",
                    "name": "kategori.kategori_nama",
                    "orderable": true,
                    "searchable": true,
                    "title": "Kategori",
                    "class": "auto text-uppercase all"
                },
                {
                    "data": "reg_nomor",
                    "name": "reg_nomor",
                    "orderable": true,
                    "searchable": true,
                    "title": "Nomor",
                    "class": "auto text-uppercase"
                },
                {
                    "data": "reg_tahun",
                    "name": "reg_tahun",
                    "orderable": true,
                    "searchable": true,
                    "title": "Tahun",
                    "class": "auto text-uppercase"
                },
               /* {
                    "data": "reg_tanggal",
                    "name": "reg_tanggal",
                    "orderable": true,
                    "searchable": true,
                    "title": "Tanggal",
                    "class": "auto text-uppercase"
                },*/
                {
                    "data": "reg_judul",
                    "name": "reg_judul",
                    "orderable": true,
                    "searchable": true,
                    "title": "Judul",
                    "class": "all text-uppercase"
                },
                {
                    "data": "reg_filename",
                    "name": "reg_filename",
                    "orderable": true,
                    "searchable": true,
                    "title": "File PDF",
                    "class": "auto text-capitalize"
                },
            ]

        };

        $(document).ready(function () {
            window.dt = $('#datatables').DataTable(configDt)
        });

        $("#tahun").on("change", function (e) {
            configDt.ajax.data.tahun = $(this).val();
            dt.destroy();
            dt = $('#datatables').DataTable(configDt)
        });
        $("#kategori").on("change", function (e) {
            configDt.ajax.data.kategori = $(this).val();
            dt.destroy();
            dt = $('#datatables').DataTable(configDt)
        });
    </script>
@endpush
