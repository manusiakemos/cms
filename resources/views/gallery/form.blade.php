<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" enctype="multipart/form-data">
            @csrf
            @isset($method)
                @if($method == "PUT")
                    @method("PUT")
                @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="judul">Nama</label>
                        <input name="judul" type="text"
                               data-parsley-errors-container="#judul-errors"
                               value="<?php echo e(isset($data) ? $data->judul : ""); ?>"
                               class="form-control" id="judul">
                        <div id="judul-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input name="keterangan" type="text"
                               data-parsley-errors-container="#keterangan-errors"
                               value="<?php echo e(isset($data) ? $data->keterangan : ""); ?>"
                               class="form-control" id="keterangan">
                        <div id="keterangan-errors"></div>
                    </div>
                    {{--<div class="form-group">
                        <label for="slug">Slug</label>
                        <input name="slug" type="text"
                               data-parsley-errors-container="#slug-errors"
                               value="<?php echo e(isset($data) ? $data->slug : ""); ?>"
                               class="form-control" id="slug">
                        <div id="slug-errors"></div>
                    </div>--}}
                    <div class="form-group">
                        <div class="custom-file mt-4">
                            <input name="filename" type="file"
                                   class="custom-file-input" id="filename">
                            <label class="custom-file-label" for="filename">File</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
