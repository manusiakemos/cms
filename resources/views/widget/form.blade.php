<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" >
            @csrf
            @isset($method)
            @if($method == "PUT")
            @method("PUT")
            @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
            <label for="name">Nama</label>
            <input name="name" type="text"
                   data-parsley-errors-container="#name-errors"
                   value="<?php echo e(isset($data) ? $data->name : ""); ?>"
                   class="form-control" id="name">
            <div id="name-errors"></div>
        </div>
                    <div class="form-group">
            <div class="custom-file mt-4">
                <input name="icon" type="file"
                       class="custom-file-input" id="icon">
                <label class="custom-file-label" for="icon">Icon</label>
            </div>
        </div>
                    <div class="form-group">
            <label for="html">Html</label>
            <textarea name="html" type="text"
                   data-parsley-errors-container="#html-errors"
                      class="form-control" id="html"><?php echo e(isset($data) ? $data->html : ""); ?></textarea>
            <div id="html-errors"></div>
        </div>
                   {{-- <div class="form-group">
            <label for="fixed">Fixed</label>
            <input name="fixed" type="text"
                   data-parsley-errors-container="#fixed-errors"
                   value="<?php echo e(isset($data) ? $data->fixed : ""); ?>"
                   class="form-control" id="fixed">
            <div id="fixed-errors"></div>
        </div>--}}
                    <div class="form-group">
            <label for="css_cdn">CSS CDN</label>
            <input name="css_cdn" type="text"
                   data-parsley-errors-container="#css_cdn-errors"
                   value="<?php echo e(isset($data) ? $data->css_cdn : ""); ?>"
                   class="form-control" id="css_cdn">
            <div id="css_cdn-errors"></div>
        </div>
                    <div class="form-group">
            <label for="js_cdn">JS CDN</label>
            <input name="js_cdn" type="text"
                   data-parsley-errors-container="#js_cdn-errors"
                   value="<?php echo e(isset($data) ? $data->js_cdn : ""); ?>"
                   class="form-control" id="js_cdn">
            <div id="js_cdn-errors"></div>
        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
