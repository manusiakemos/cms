<div class="custom-control custom-checkbox">
    <input type="checkbox" id="{{ $data->reg_id }}" name="checked[]" class="custom-control-input dt-selectable" value="true">
    <label class="custom-control-label" for="{{ $data->reg_id }}"></label>
</div>
