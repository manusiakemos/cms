<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{$action}}" id="formModal" method="POST" enctype="multipart/form-data">
            @csrf
            @isset($method)
            @if($method == "PUT")
            @method("PUT")
            @endif
            @endisset
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{isset($modal_title) ? $modal_title : ""}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
            <label for="kategori_id">Kategori</label>
            <x-select-component id="kategori_id"
               name="kategori_id"
               error-container="#kategori_id-errors"
               case="status"
               :selected="isset($data) ? $data->kategori_id : 1">
            </x-select-component>
            <div id="kategori_id-errors"></div>
        </div>
                    <div class="form-group">
            <label for="reg_judul">Judul</label>
            <input name="reg_judul" type="text"
                   data-parsley-errors-container="#reg_judul-errors"
                   value="<?php echo e(isset($data) ? $data->reg_judul : ""); ?>"
                   class="form-control" id="reg_judul">
            <div id="reg_judul-errors"></div>
        </div>
                    <div class="form-group">
            <label for="reg_nomor">Nomor</label>
            <input name="reg_nomor" type="text"
                   data-parsley-errors-container="#reg_nomor-errors"
                   value="<?php echo e(isset($data) ? $data->reg_nomor : ""); ?>"
                   class="form-control" id="reg_nomor">
            <div id="reg_nomor-errors"></div>
        </div>
                    <div class="form-group">
            <label for="reg_tahun">Tahun</label>
            <input name="reg_tahun" type="text"
                   placeholder="YYYY-MM-DD"
                   data-parsley-errors-container="#reg_tahun-errors"
                   value="<?php echo e(isset($data) ? $data->reg_tahun : date('Y')); ?>"
                   class="form-control year" id="reg_tahun">
            <div id="reg_tahun-errors"></div>
        </div>
                    <div class="form-group">
            <label for="reg_tanggal">Tanggal</label>
            <input name="reg_tanggal" type="text"
                   placeholder="YYYY-MM-DD"
                   data-parsley-errors-container="#reg_tanggal-errors"
                   value="<?php echo e(isset($data) ? $data->reg_tanggal : '0000-00-00'); ?>"
                   class="form-control date" id="reg_tanggal">
            <div id="reg_tanggal-errors"></div>
        </div>
                    <div class="form-group">
            <div class="custom-file mt-4">
                <input name="reg_filename" type="file"
                       class="custom-file-input" id="reg_filename">
                <label class="custom-file-label" for="reg_filename">File PDF</label>
            </div>
        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{{asset('vendor/crudgen/libs/etc/init.js')}}"></script>
<script>
    $("#formModal").on("submit", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var serializedArray = $(this).serializeArray();
        $.each(serializedArray, function (index, value) {
            let el = $(`#${value.name}-errors`);
            el.html("");
        });
        $(this).ajaxSubmit({
            success: function (response) {
                alertify.success(response.message);
                dt.ajax.reload(null, false);
            },
            error: function (error) {
                alertify.error(error.responseJSON.message);
                var errorBags = error.responseJSON.errors;
                console.log(errorBags);
                $.each(errorBags, function (index, value) {
                    let el = $(`#${index}-errors`);
                    el.html(`<small class='text-danger'>${value.join()}</small>`);
                });
            }
        });
        return false;
    });
</script>
