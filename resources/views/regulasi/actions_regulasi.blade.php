<div class="btn-group" role="group">
    @if(auth()->check())
        <a href="{{route('regulasi.edit', $data->reg_id)}}" class="btn btn-primary btn-edit btn-sm">
            <span class="material-icons" style="font-size: 0.9rem">edit</span> Edit
        </a>
        <a href="{{route('regulasi.destroy', $data->reg_id)}}" class="btn btn-danger btn-destroy btn-sm">
            <span class="material-icons" style="font-size: 0.9rem">delete</span>
            Hapus
        </a>
    @endif
    <a href="{{route('view.regulasi', [$data->reg_nomor,$data->reg_tahun])}}" class="btn btn-danger btn-sm">
        <span class="material-icons" style="font-size: 0.9rem">picture_as_pdf</span>
        Lihat PDF
    </a>
</div>
