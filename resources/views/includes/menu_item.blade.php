<li>
    <a href="#">{{$menu->text}}</a>
    <ul>
        @foreach($menu->sub as $child)
           @isset($child)
                @if($child->type == 'link')
                    <li><a href="{{isset($child->data) ? url($child->data) : ''}}">{{$child->text}}</a></li>
                @elseif($child->type == 'halaman')
                    <li><a href="{{url('/halaman/'.$child->data)}}">{{$child->text}}</a></li>
                @elseif($child->type == 'dropdown')
                    @include('includes.menu_item', ['menu' => $child])
                @endif
            @endisset
        @endforeach
    </ul>
</li>

