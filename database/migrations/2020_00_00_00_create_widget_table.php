<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget', function (Blueprint $table) {
            $table->increments('id')
                            ;
                            $table->string('name' ,190)
                ->nullable()            ;
                            $table->string('icon' ,190)
                ->nullable()            ;
                $table->longText('html')
        ->nullable()            ;
                $table->boolean('fixed')
        ->nullable()            ;
                            $table->string('css_cdn' ,190)
                ->nullable()            ;
                            $table->string('js_cdn' ,190)
                ->nullable()            ;
    
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget');
    }
}
