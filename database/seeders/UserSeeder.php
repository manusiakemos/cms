<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insert = [
            [
                'name' => "admin",
                'username' => "masuk",
                'email' => "sa@admin.com",
                'email_verified_at' => now(),
                'password' => bcrypt("masukhaja"), // password
                'remember_token' => Str::random(10),
                'api_token' => Str::random(60),
                'role' => 'admin'
            ],
            [
                'name' => "super admin",
                'username' => "super-admin",
                'email' => "admin@admin.com",
                'email_verified_at' => now(),
                'password' => bcrypt("samasukhaja"), // password
                'remember_token' => Str::random(10),
                'api_token' => Str::random(60),
                'role' => 'super-admin'
            ]
        ];
        DB::table("users")->insert($insert);
    }
}
