<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('setting_group')->insert(array (
            array (
                'id' => 1,
                'group' => 'core',
                'deleted_at' => NULL,
                'created_at' => '2021-03-19 09:19:59',
                'updated_at' => '2021-03-19 09:19:59',
            ),
            array (
                'id' => 2,
                'group' => 'ketua',
                'deleted_at' => NULL,
                'created_at' => '2021-03-19 09:19:59',
                'updated_at' => '2021-03-19 09:19:59',
            ),
        ));


    }
}
