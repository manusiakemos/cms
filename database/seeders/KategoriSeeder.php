<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kategori as KategoriAlias;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriAlias::insert([
            [
                'kategori_nama' => 'Keputusan Bupati',
                'kategori_aktif' => 1,
            ],
            [
                'kategori_nama' => 'Peraturan Bupati',
                'kategori_aktif' => 1,
            ],
            [
                'kategori_nama' => 'Peraturan Daerah',
                'kategori_aktif' => 1,
            ]
        ]);
    }
}
