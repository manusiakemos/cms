<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $insert = [
            [
                'group_id' => '1',
                'name' => 'nama_aplikasi_alt',
                'value' => 'NAMA APLIKASI ALT',
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'text',
                'deleted_at' => NULL,

            ],
            [
                'group_id' => '1',
                'name' => 'logo_alt',
                'value' => NULL,
                'image' => 'yJha1Suy3rRe4Pwi.png',
                'fixed' => 1,
                'active' => 1,
                'type' => 'image',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '1',
                'name' => 'logo',
                'value' => NULL,
                'image' => 'O8FfNghRQ6up8Qus.png',
                'fixed' => 1,
                'active' => 1,
                'type' => 'image',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '1',
                'name' => 'nama_aplikasi',
                'value' => 'NAMA APLIKASI',
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'text',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '1',
                'name' => 'google_maps',
                'value' => '',
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'textarea',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '2',
                'name' => 'ketua_image',
                'value' => NULL,
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'image',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '2',
                'name' => 'ketua_nama',
                'value' => NULL,
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'text',
                'deleted_at' => NULL,
            ],
            [
                'group_id' => '2',
                'name' => 'ketua_jabatan',
                'value' => NULL,
                'image' => NULL,
                'fixed' => 1,
                'active' => 1,
                'type' => 'text',
                'deleted_at' => NULL,
            ],
        ];
        DB::table('setting')->insert($insert);
    }
}
